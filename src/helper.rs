use super::glenum::*;

pub(crate) fn bytes_per_pixel(format: PixelFormat, pixel_type: PixelType) -> u32 {
    use PixelType::*;
    use PixelFormat::*;
    let units = match format {
        RGB   => 3,
        RGBA  => 4,
        Alpha => 1,
        Luminance => 1,
        LuminanceAlpha => 1,
    };
    match pixel_type {
        U8 => 1 * units,
        U16_4_4_4_4 => 2,
        U16_5_5_5_1 => 2,
        U16_5_6_5   => 2,
        Float => 4 * units,
    }
}

pub(crate) fn slice_length<T: Sized>(data: &[T]) -> usize {
    std::mem::size_of::<T>() * data.len()
}
