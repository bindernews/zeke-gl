use super::*;
use super::glenum::*;
use std::slice::from_raw_parts;

#[derive(Clone, Debug)]
pub struct WebGLActiveInfo {
    pub name: String,
    pub size: u32,
    pub kind: ActiveType,
}

#[derive(Clone)]
pub enum WebGLParameterOut {
    Enum(u32),
    U32(u32),
    I32(i32),
    F32(f32),
    Bool(bool),
    Buffer(WebGLBuffer),
    Program(WebGLProgram),
    Framebuffer(WebGLFramebuffer),
    Renderbuffer(WebGLRenderbuffer),
    Texture(WebGLTexture),
}

pub trait IGLContext {
    fn active_texture(&self, unit: u32);
    fn attach_shader(&self, program: &WebGLProgram, shader: &WebGLShader);
    fn bind_attrib_location(&self, program: &WebGLProgram, index: u32, name: &str);

    fn bind_buffer(&self, kind: BufferKind, buffer: Option<&WebGLBuffer>);
    fn bind_framebuffer(&self, fb: Option<&WebGLFramebuffer>);
    fn bind_renderbuffer(&self, rb: Option<&WebGLRenderbuffer>);
    fn bind_texture(&self, target: TextureTarget, tex: Option<&WebGLTexture>);
    fn bind_vertex_array(&self, vao: Option<&WebGLVertexArray>);

    fn blend_color(&self, r: f32, g: f32, b: f32, a: f32);

    fn blend_equation(&self, mode: BlendMode) {
        self.blend_equation_separate(mode, mode)
    }

    fn blend_equation_separate(&self, mode_rgb: BlendMode, mode_alpha: BlendMode);
    fn blend_func(&self, sfactor: BlendFactor, dfactor: BlendFactor);
    fn blend_func_separate(&self, src_rgb: BlendFactor, dst_rgb: BlendFactor,
                        src_a: BlendFactor, dst_a: BlendFactor);
    fn buffer_data<T: Sized>(&self, kind: BufferKind, data: &[T], usage: BufferUsage);
    fn buffer_sub_data<T: Sized>(&self, kind: BufferKind, offset: u32, data: &[T]);
    fn check_framebuffer_status(&self) -> FramebufferError;
    fn clear(&self, mask: u32);
    fn clear_color(&self, r: f32, g: f32, b: f32, a: f32);
    fn clear_depth(&self, depth: f32);
    fn clear_stencil(&self, s: i32);
    fn color_mask(&self, r: bool, g: bool, b: bool, a: bool);
    fn compile_shader(&self, shader: &WebGLShader) -> Result<(), String>;
    // fn compressed_tex_image2d(...)
    // fn compressed_tex_image3d(...)
    // fn compressed_tex_sub_image2d(...)
    // fn compressed_tex_sub_image3d(...)
    fn copy_tex_image2d(&self, target: TextureTarget, level: u8, format: PixelFormat, x: u32, y: u32, width: u32, height: u32);
    // copy_tex_sub_image2d
    fn create_buffer(&self) -> WebGLBuffer;
    fn create_framebuffer(&self) -> WebGLFramebuffer;
    fn create_program(&self) -> WebGLProgram;
    fn create_renderbuffer(&self) -> WebGLRenderbuffer;
    fn create_shader(&self, kind: ShaderKind) -> WebGLShader;
    fn create_texture(&self) -> WebGLTexture;
    fn create_vertex_array(&self) -> WebGLVertexArray;
    fn cull_face(&self, mode: FaceMode);
    fn delete_buffer(&self, buffer: &WebGLBuffer);
    fn delete_framebuffer(&self, fb: &WebGLFramebuffer);
    fn delete_program(&self, program: &WebGLProgram);
    fn delete_renderbuffer(&self, rb: &WebGLRenderbuffer);
    fn delete_shader(&self, shader: &WebGLShader);
    fn delete_texture(&self, tex: &WebGLTexture);
    fn delete_vertex_array(&self, vao: &WebGLVertexArray);
    fn depth_func(&self, func: CompareOp);
    fn depth_mask(&self, flag: bool);
    fn depth_range(&self, z_near: f32, z_far: f32);
    fn detach_shader(&self, program: &WebGLProgram, shader: &WebGLShader);
    fn disable(&self, flag: Flag);
    fn disable_vertex_attrib_array(&self, loc: u32);
    fn draw_arrays(&self, mode: Primitives, first: u32, count: u32);
    fn draw_elements(&self, mode: Primitives, count: usize, kind: DataType, offset: u32);
    fn enable(&self, flag: Flag);
    fn enable_vertex_attrib_array(&self, index: u32);
    fn finish(&self);
    fn flush(&self);
    fn framebuffer_renderbuffer(&self, attachment: Attachment, rb: &WebGLRenderbuffer);
    fn framebuffer_texture2d(&self, attachment: Attachment, textarget: TextureTarget, 
                            texture: &WebGLTexture, level: i32);
    fn front_face(&self, mode: WindingMode);
    fn generate_mipmap(&self, target: TextureTarget);
    fn get_active_attrib(&self, program: &WebGLProgram, index: u32) -> Option<WebGLActiveInfo>;
    fn get_active_uniform(&self, program: &WebGLProgram, index: u32) -> Option<WebGLActiveInfo>;
    // fn get_attached_shaders(...)
    fn get_attrib_location(&self, program: &WebGLProgram, name: &str) -> Option<u32>;
    // fn get_buffer_parameter(...)
    // fn get_context_attributes(...)
    fn get_error(&self) -> u32;
    // fn get_framebuffer_attachment_parameter(...)
    fn get_parameter(&self, parameter: u32) -> WebGLParameterOut;
    // fn get_program_info_log(...)
    fn get_program_parameter(&self, program: &WebGLProgram, param: ProgramParameter) -> i32;
    // fn get_renderbuffer_parameter(...)
    // fn get_shader_info_log(...)
    // fn get_shader_parameter(...)
    // fn get_shader_precision_format(...)
    // fn get_shader_source(...)
    // fn get_supported_extensions(...)
    // fn get_tex_parameter(...)
    // fn get_uniform(...)
    fn get_uniform_location(&self, program: &WebGLProgram, name: &str) -> Option<WebGLUniformLocation>;
    // fn get_vertex_attrib(...)
    // fn get_vertex_attrib_offset(...)
    // fn hint(...)
    // fn is_...(...)
    // fn line_width(...)
    fn link_program(&self, program: &WebGLProgram) -> Result<(), String>;
    // fn pixel_storei(...)
    // fn polygon_offset(...)
    fn read_pixels(&self, x: u32, y: u32, width: u32, height: u32,
                   format: PixelFormat, pixel_type: PixelType, data: &mut [u8]);
    fn renderbuffer_storage(&self, format: RenderbufferFormat, width: u32, height: u32);
    // fn sample_coverage(...)
    fn scissor(&self, x: u32, y: u32, w: u32, h: u32);
    fn shader_source(&self, shader: &WebGLShader, source: &str);
    // fn stencil_func(...)
    fn stencil_func_separate(&self, face: FaceMode, func: CompareOp, ref_value: i32, mask: u32);
    // fn stencil_mask(...)
    fn stencil_mask_separate(&self, face: FaceMode, mask: u32);
    // fn stencil_op(...)
    fn stencil_op_separate(&self, face: FaceMode, sfail: StencilOp, zfail: StencilOp, zpass: StencilOp);
    fn tex_image2d<T: Sized>(&self, target: TextureTarget, level: u8, format: PixelFormat,
                             width: u32, height: u32, pixel_type: PixelType, data: &[T]);
    fn tex_parameter(&self, target: TextureTarget, name: ParamName, value: ParamValue);
    fn tex_sub_image2d<T: Sized>(&self, target: TextureTarget, level: u8,
                                xoffset: u32, yoffset: u32, width: u32, height: u32,
                                format: PixelFormat, pixel_type: PixelType, data: &[T]);

    fn uniform_1f(&self, loc: &WebGLUniformLocation, value: f32);
    fn uniform_1i(&self, loc: &WebGLUniformLocation, value: i32);
    fn uniform_2f(&self, loc: &WebGLUniformLocation, value: (f32, f32));
    fn uniform_2i(&self, loc: &WebGLUniformLocation, value: (i32, i32));
    fn uniform_3f(&self, loc: &WebGLUniformLocation, value: (f32, f32, f32));
    fn uniform_3i(&self, loc: &WebGLUniformLocation, value: (i32, i32, i32));
    fn uniform_4f(&self, loc: &WebGLUniformLocation, value: (f32, f32, f32, f32));
    fn uniform_4i(&self, loc: &WebGLUniformLocation, value: (i32, i32, i32, i32));
    fn uniform_matrix_2f(&self, loc: &WebGLUniformLocation, value: &[f32]);
    fn uniform_matrix_3f(&self, loc: &WebGLUniformLocation, value: &[f32]);
    fn uniform_matrix_4f(&self, loc: &WebGLUniformLocation, value: &[f32]);
    fn use_program(&self, program: &WebGLProgram);
    // fn validate_program(...)
    // vertex_attrib_...(...)
    fn vertex_attrib_pointer(&self, index: u32, size: u8, kind: DataType, normalized: bool, 
                            stride: u32, offset: u32);
    fn viewport(&self, x: u32, y: u32, w: u32, h: u32);
}

impl WebGLRenderingContext {
    #[inline]
    pub fn bind_buffer(&self, kind: BufferKind, buffer: &WebGLBuffer) {
        self.ctx.bind_buffer(kind, Some(buffer))
    }

    #[inline]
    pub fn bind_framebuffer(&self, fb: &WebGLFramebuffer) {
        self.ctx.bind_framebuffer(Some(fb))
    }
    
    #[inline]
    pub fn bind_renderbuffer(&self, rb: &WebGLRenderbuffer) {
        self.ctx.bind_renderbuffer(Some(rb))
    }

    #[inline]
    pub fn bind_texture(&self, target: TextureTarget, tex: &WebGLTexture) {
        self.ctx.bind_texture(target, Some(tex))
    }

    #[inline]
    pub fn bind_vertex_array(&self, vao: &WebGLVertexArray) {
        self.ctx.bind_vertex_array(Some(vao))
    }

    pub fn clear_color_u8(&self, r: u8, g: u8, b: u8, a: u8) {
        self.ctx.clear_color(r as f32 / 255., g as f32 / 255., b as f32 / 255., a as f32 / 255.);
    }

    #[inline]
    pub fn uniform_matrix_2fm(&self, loc: &WebGLUniformLocation, value: &[[f32; 2]; 2]) {
        self.ctx.uniform_matrix_2f(loc, unsafe { from_raw_parts(value.as_ptr() as *const f32, 4) })
    }

    #[inline]
    pub fn uniform_matrix_3fm(&self, loc: &WebGLUniformLocation, value: &[[f32; 3]; 3]) {
        self.ctx.uniform_matrix_3f(loc, unsafe { from_raw_parts(value.as_ptr() as *const f32, 9) })
    }

    #[inline]
    pub fn uniform_matrix_4fm(&self, loc: &WebGLUniformLocation, value: &[[f32; 4]; 4]) {
        self.ctx.uniform_matrix_4f(loc, unsafe { from_raw_parts(value.as_ptr() as *const f32, 16) })
    }

    #[inline]
    pub fn unbind_buffer(&self, kind: BufferKind) {
        self.ctx.bind_buffer(kind, None)
    }

    #[inline]
    pub fn unbind_framebuffer(&self) {
        self.ctx.bind_framebuffer(None)
    }

    #[inline]
    pub fn unbind_renderbuffer(&self) {
        self.ctx.bind_renderbuffer(None)
    }

    #[inline]
    pub fn unbind_texture(&self, target: TextureTarget) {
        self.ctx.bind_texture(target, None)
    }

    #[inline]
    pub fn unbind_vertex_array(&self) {
        self.ctx.bind_vertex_array(None)
    }
}
