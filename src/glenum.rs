#[allow(non_camel_case_types)]

macro_rules! implement_try_from_u32 {
    ($atype:ident; $($field:ident),*) => {
        impl $atype {
            pub fn from_u32(value: u32) -> Result<Self, &'static str> {
                let x: $atype = unsafe { ::std::mem::transmute(value) };
                use $atype::*;
                match x {
                    $(
                        $field => Ok($field),
                    )*
                    //_ => Err("Invalid enum value"),
                }
            }
        }
    };
}

use enumerations::*;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u32)]
pub enum Flag {
    Blend = GL_BLEND,
    CullFace = GL_CULL_FACE,
    DepthTest = GL_DEPTH_TEST,
    Dither = GL_DITHER,
    PolygonOffsetFill = GL_POLYGON_OFFSET_FILL,
    SampleAlphaToCoverage = GL_SAMPLE_ALPHA_TO_COVERAGE,
    SampleCoverage = GL_SAMPLE_COVERAGE,
    ScissorTest = GL_SCISSOR_TEST,
    StencilTest = GL_STENCIL_TEST,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u32)]
pub enum BufferKind {
    Array = 0x8892,
    Index = 0x8893,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u32)]
pub enum BufferUsage {
    Dynamic = 0x88E8,
    Static  = 0x88E4,
    Stream  = 0x88E0,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u32)]
pub enum ShaderKind {
    Vertex   = 0x8B31,
    Fragment = 0x8B30,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u32)]
pub enum AttributeSize {
    One   = 1,
    Two   = 2,
    Three = 3,
    Four  = 4,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u32)]
pub enum DataType {
    I8 = 0x1400,
    U8 = 0x1401,
    I16 = 0x1402,
    U16 = 0x1403,
    I32 = 0x1404,
    U32 = 0x1405,
    Float = 0x1406,
}

#[derive(Debug, Clone, Eq, PartialEq, Copy)]
#[repr(u32)]
pub enum ActiveType {
    Float = 0x1406,
    FloatMat2 = 0x8B5A,
    FloatMat3 = 0x8B5B,
    FloatMat4 = 0x8B5C,
    FloatVec2 = 0x0B50,
    FloatVec3 = 0x8B51,
    FloatVec4 = 0x8B52,
    Int = 0x1404,
    IntVec2   = 0x8B53,
    IntVec3   = 0x8B54,
    IntVec4   = 0x8B55,
}

impl ActiveType {
    pub fn elements(self) -> u8 {
        use ActiveType::*;
        match self {
            Float => 1,
            FloatVec2 => 2,
            FloatVec3 => 3,
            FloatVec4 => 4,
            FloatMat2 => 4,
            FloatMat3 => 9,
            FloatMat4 => 16,
            Int => 1,
            IntVec2 => 2,
            IntVec3 => 3,
            IntVec4 => 4,
        }
    }
}
implement_try_from_u32!{ ActiveType;
    Float, FloatMat2, FloatMat3, FloatMat4, FloatVec2, FloatVec3, FloatVec4, Int, IntVec2, IntVec3, IntVec4
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u32)]
pub enum ProgramParameter {
    ActiveAttributes = GL_ACTIVE_ATTRIBUTES,
    ActiveUniforms = GL_ACTIVE_UNIFORMS,
}

/// Passed to drawElements or drawArrays to draw primitives.
#[derive(Debug, Clone, Copy)]
pub enum Primitives {
    /// Passed to drawElements or drawArrays to draw single points.
    Points = 0x0000,
    /// Passed to drawElements or drawArrays to draw lines. Each vertex connects to the one after it.
    Lines = 0x0001,
    /// Passed to drawElements or drawArrays to draw lines. Each set of two vertices is treated as a separate line segment.
    LineLoop = 0x0002,
    /// Passed to drawElements or drawArrays to draw a connected group of line segments from the first vertex to the last.
    LineStrip = 0x0003,
    /// Passed to drawElements or drawArrays to draw triangles. Each set of three vertices creates a separate triangle.
    Triangles = 0x0004,
    /// Passed to drawElements or drawArrays to draw a connected group of triangles.
    TriangleStrip = 0x0005,
    /// Passed to drawElements or drawArrays to draw a connected group of triangles. Each vertex connects to the previous and the first vertex in the fan.
    TriangleFan = 0x0006,
}

#[derive(Debug, Clone, Copy)]
pub enum FramebufferTarget {
    Framebuffer = 0x8D40,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(u32)]
pub enum FramebufferError {
    Complete = GL_FRAMEBUFFER_COMPLETE,
    IncompleteAttachment = GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT,
    MissingAttachment = GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT,
    Unsupported = GL_FRAMEBUFFER_UNSUPPORTED,
    Unknown = 0,
}

impl FramebufferError {
    pub fn from_u32(code: u32) -> FramebufferError {
        use FramebufferError::*;
        match code {
            GL_FRAMEBUFFER_COMPLETE => Complete,
            GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT => IncompleteAttachment,
            GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT => MissingAttachment,
            GL_FRAMEBUFFER_UNSUPPORTED => Unsupported,
            _ => Unknown,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Attachment {
    ColorAttachment0       = 0x8CE0,
    DepthAttachment        = 0x8D00,
    StencilAttachment      = 0x8D20,
}

#[derive(Copy, Clone, Debug)]
pub enum RenderbufferFormat {
    RGBA4   = 0x8056,
    RGB565  = 0x8D62,
    RGB5A1  = 0x8057,
    Depth16 = 0x81A5,
}

#[derive(Copy, Clone, Debug)]
pub enum PixelFormat {
    RGB   = 0x1907,
    RGBA  = 0x1908,
    Alpha = 0x1906,
    Luminance      = 0x1909,
    LuminanceAlpha = 0x190A,
}

#[derive(Copy, Clone, Debug)]
pub enum TextureTarget {
    Texture2D   = 0x0DE1,
    CubeMap     = 0x8513,
    CubeMapPosX = 0x8515,
    CubeMapNegX = 0x8516,
    CubeMapPosY = 0x8517,
    CubeMapNegY = 0x8518,
    CubeMapPosZ = 0x8519,
    CubeMapNegZ = 0x851A,
}

#[derive(Copy, Clone, Debug)]
pub enum PixelType {
    U8 = 0x1401,
    Float = 0x1406,
    U16_4_4_4_4 = 0x8033,
    U16_5_5_5_1 = 0x8034,
    U16_5_6_5 = 0x8363,
}

#[derive(Copy, Clone, Debug)]
pub enum ParamName {
    TextureMagFilter = 0x2800,
    TextureMinFilter = 0x2801,
    TextureWrapS = 0x2802,
    TextureWrapT = 0x2803,
}

#[derive(Copy, Clone, Debug)]
pub enum ParamValue {
    Linear  = 0x2601,
    Nearest = 0x2600,
    Repeat  = 0x2901,
    ClampToEdge = 0x812F,
    MirroredRepeat = 0x8370,
}

#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum FaceMode {
    Front = 0x0404,
    Back  = 0x0405,
    FrontAndBack = 0x0408,
}

#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum WindingMode {
    CW  = 0x0900,
    CCW = 0x0901,
}

#[derive(Copy, Clone, Debug)]
pub enum CompareOp {
    Never    = 0x0200,
    Less     = 0x0201,
    Equal    = 0x0202,
    LEqual   = 0x0203,
    Greater  = 0x0204,
    NotEqual = 0x0205,
    GEqual   = 0x0206,
    Always   = 0x0207,
}

#[derive(Copy, Clone, Debug)]
pub enum StencilOp {
    Keep     = 0x1E00,
    Zero     = 0x0000,
    Replace  = 0x1E01,
    Incr     = 0x1E02,
    IncrWrap = 0x8507,
    Decr     = 0x1E03,
    DecrWrap = 0x8508,
    Invert   = 0x150A,
}

#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum ClearMask {
    ColorBuffer   = 0x00004000,
    DepthBuffer   = 0x00000100,
    StencilBuffer = 0x00000400,
}

#[derive(Copy, Clone, Debug)]
pub enum BlendMode {
    FuncAdd  = 0x8006,
    FuncSubtract = 0x800A,
    FuncReverseSubtract = 0x800B,
}

#[derive(Copy, Clone, Debug)]
pub enum BlendFactor {
    Zero = 0x0000,
    One  = 0x0001,
    SrcColor = 0x0300,
    OneMinusSrcColor = 0x0301,
    DstColor = 0x0306,
    OneMinusDstColor = 0x0307,
    SrcAlpha = 0x0302,
    OneMinusSrcAlpha = 0x0303,
    DstAlpha = 0x0304,
    OneMinusDstAlpha = 0x0305,
    ConstColor = 0x8001,
    OneMinusConstColor = 0x8002,
    ConstAlpha = 0x8003,
    OneMinusConstAlpha = 0x8004,
    SrcAlphaSaturate = 0x0308,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
#[repr(u32)]
pub enum Severity {
    High = GL_DEBUG_SEVERITY_HIGH_KHR,
    Medium = GL_DEBUG_SEVERITY_MEDIUM_KHR,
    Low = GL_DEBUG_SEVERITY_LOW_KHR,
    Info = GL_DEBUG_SEVERITY_NOTIFICATION_KHR,
    Unknown = 0,
}

#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum ErrorCode {
    NoError          = 0x0000,
    InvalidEnum      = 0x0500,
    InvalidValue     = 0x0501,
    InvalidOperation = 0x0502,
    StackOverflow    = 0x0503,
    StackUnderflow   = 0x0504,
    OutOfMemory      = 0x0505,
    InvalidFramebufferOp = 0x0506,
}
impl ErrorCode {
    pub fn from_u32(code: u32) -> Option<ErrorCode> {
        use ErrorCode::*;
        match code {
            GL_NO_ERROR => Some(NoError),
            GL_INVALID_ENUM => Some(InvalidEnum),
            GL_INVALID_VALUE => Some(InvalidValue),
            GL_INVALID_OPERATION => Some(InvalidOperation),
            GL_STACK_OVERFLOW_KHR => Some(StackOverflow),
            GL_STACK_UNDERFLOW_KHR => Some(StackUnderflow),
            GL_OUT_OF_MEMORY => Some(OutOfMemory),
            GL_INVALID_FRAMEBUFFER_OPERATION => Some(InvalidFramebufferOp),
            _ => None,
        }
    }

    pub(crate) fn as_str(self) -> &'static str {
        use ErrorCode::*;
        match self {
            NoError => "no error",
            InvalidEnum => "invalid enum",
            InvalidValue => "invalid value",
            InvalidOperation => "invalid operation",
            StackOverflow => "stack overflow",
            StackUnderflow => "stack underflow",
            OutOfMemory => "out of memory",
            InvalidFramebufferOp => "invalid framebuffer operation",
        }
    }
}

pub mod enumerations {
    #![allow(dead_code, non_upper_case_globals, unused_imports)]
    
    pub const GL_ACTIVE_ATTRIBUTES: u32 = 0x8B89;
    pub const GL_ACTIVE_ATTRIBUTE_MAX_LENGTH: u32 = 0x8B8A;
    pub const GL_ACTIVE_TEXTURE: u32 = 0x84E0;
    pub const GL_ACTIVE_UNIFORMS: u32 = 0x8B86;
    pub const GL_ACTIVE_UNIFORM_MAX_LENGTH: u32 = 0x8B87;
    pub const GL_ALIASED_LINE_WIDTH_RANGE: u32 = 0x846E;
    pub const GL_ALIASED_POINT_SIZE_RANGE: u32 = 0x846D;
    pub const GL_ALPHA: u32 = 0x1906;
    pub const GL_ALPHA_BITS: u32 = 0x0D55;
    pub const GL_ALWAYS: u32 = 0x0207;
    pub const GL_ARRAY_BUFFER: u32 = 0x8892;
    pub const GL_ARRAY_BUFFER_BINDING: u32 = 0x8894;
    pub const GL_ATTACHED_SHADERS: u32 = 0x8B85;
    pub const GL_BACK: u32 = 0x0405;
    pub const GL_BLEND: u32 = 0x0BE2;
    pub const GL_BLEND_COLOR: u32 = 0x8005;
    pub const GL_BLEND_DST_ALPHA: u32 = 0x80CA;
    pub const GL_BLEND_DST_RGB: u32 = 0x80C8;
    pub const GL_BLEND_EQUATION: u32 = 0x8009;
    pub const GL_BLEND_EQUATION_ALPHA: u32 = 0x883D;
    pub const GL_BLEND_EQUATION_RGB: u32 = 0x8009;
    pub const GL_BLEND_SRC_ALPHA: u32 = 0x80CB;
    pub const GL_BLEND_SRC_RGB: u32 = 0x80C9;
    pub const GL_BLUE_BITS: u32 = 0x0D54;
    pub const GL_BOOL: u32 = 0x8B56;
    pub const GL_BOOL_VEC2: u32 = 0x8B57;
    pub const GL_BOOL_VEC3: u32 = 0x8B58;
    pub const GL_BOOL_VEC4: u32 = 0x8B59;
    pub const GL_BUFFER_KHR: u32 = 0x82E0;
    pub const GL_BUFFER_SIZE: u32 = 0x8764;
    pub const GL_BUFFER_USAGE: u32 = 0x8765;
    pub const GL_BYTE: u32 = 0x1400;
    pub const GL_CCW: u32 = 0x0901;
    pub const GL_CLAMP_TO_EDGE: u32 = 0x812F;
    pub const GL_COLOR_ATTACHMENT0: u32 = 0x8CE0;
    pub const GL_COLOR_BUFFER_BIT: u32 = 0x00004000;
    pub const GL_COLOR_CLEAR_VALUE: u32 = 0x0C22;
    pub const GL_COLOR_WRITEMASK: u32 = 0x0C23;
    pub const GL_COMPILE_STATUS: u32 = 0x8B81;
    pub const GL_COMPRESSED_TEXTURE_FORMATS: u32 = 0x86A3;
    pub const GL_CONSTANT_ALPHA: u32 = 0x8003;
    pub const GL_CONSTANT_COLOR: u32 = 0x8001;
    pub const GL_CONTEXT_FLAG_DEBUG_BIT_KHR: u32 = 0x00000002;
    pub const GL_CULL_FACE: u32 = 0x0B44;
    pub const GL_CULL_FACE_MODE: u32 = 0x0B45;
    pub const GL_CURRENT_PROGRAM: u32 = 0x8B8D;
    pub const GL_CURRENT_VERTEX_ATTRIB: u32 = 0x8626;
    pub const GL_CW: u32 = 0x0900;
    pub const GL_DEBUG_CALLBACK_FUNCTION_KHR: u32 = 0x8244;
    pub const GL_DEBUG_CALLBACK_USER_PARAM_KHR: u32 = 0x8245;
    pub const GL_DEBUG_GROUP_STACK_DEPTH_KHR: u32 = 0x826D;
    pub const GL_DEBUG_LOGGED_MESSAGES_KHR: u32 = 0x9145;
    pub const GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH_KHR: u32 = 0x8243;
    pub const GL_DEBUG_OUTPUT_KHR: u32 = 0x92E0;
    pub const GL_DEBUG_OUTPUT_SYNCHRONOUS_KHR: u32 = 0x8242;
    pub const GL_DEBUG_SEVERITY_HIGH_KHR: u32 = 0x9146;
    pub const GL_DEBUG_SEVERITY_LOW_KHR: u32 = 0x9148;
    pub const GL_DEBUG_SEVERITY_MEDIUM_KHR: u32 = 0x9147;
    pub const GL_DEBUG_SEVERITY_NOTIFICATION_KHR: u32 = 0x826B;
    pub const GL_DEBUG_SOURCE_API_KHR: u32 = 0x8246;
    pub const GL_DEBUG_SOURCE_APPLICATION_KHR: u32 = 0x824A;
    pub const GL_DEBUG_SOURCE_OTHER_KHR: u32 = 0x824B;
    pub const GL_DEBUG_SOURCE_SHADER_COMPILER_KHR: u32 = 0x8248;
    pub const GL_DEBUG_SOURCE_THIRD_PARTY_KHR: u32 = 0x8249;
    pub const GL_DEBUG_SOURCE_WINDOW_SYSTEM_KHR: u32 = 0x8247;
    pub const GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_KHR: u32 = 0x824D;
    pub const GL_DEBUG_TYPE_ERROR_KHR: u32 = 0x824C;
    pub const GL_DEBUG_TYPE_MARKER_KHR: u32 = 0x8268;
    pub const GL_DEBUG_TYPE_OTHER_KHR: u32 = 0x8251;
    pub const GL_DEBUG_TYPE_PERFORMANCE_KHR: u32 = 0x8250;
    pub const GL_DEBUG_TYPE_POP_GROUP_KHR: u32 = 0x826A;
    pub const GL_DEBUG_TYPE_PORTABILITY_KHR: u32 = 0x824F;
    pub const GL_DEBUG_TYPE_PUSH_GROUP_KHR: u32 = 0x8269;
    pub const GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_KHR: u32 = 0x824E;
    pub const GL_DECR: u32 = 0x1E03;
    pub const GL_DECR_WRAP: u32 = 0x8508;
    pub const GL_DELETE_STATUS: u32 = 0x8B80;
    pub const GL_DEPTH_ATTACHMENT: u32 = 0x8D00;
    pub const GL_DEPTH_BITS: u32 = 0x0D56;
    pub const GL_DEPTH_BUFFER_BIT: u32 = 0x00000100;
    pub const GL_DEPTH_CLEAR_VALUE: u32 = 0x0B73;
    pub const GL_DEPTH_COMPONENT: u32 = 0x1902;
    pub const GL_DEPTH_COMPONENT16: u32 = 0x81A5;
    pub const GL_DEPTH_FUNC: u32 = 0x0B74;
    pub const GL_DEPTH_RANGE: u32 = 0x0B70;
    pub const GL_DEPTH_TEST: u32 = 0x0B71;
    pub const GL_DEPTH_WRITEMASK: u32 = 0x0B72;
    pub const GL_DITHER: u32 = 0x0BD0;
    pub const GL_DONT_CARE: u32 = 0x1100;
    pub const GL_DST_ALPHA: u32 = 0x0304;
    pub const GL_DST_COLOR: u32 = 0x0306;
    pub const GL_DYNAMIC_DRAW: u32 = 0x88E8;
    pub const GL_ELEMENT_ARRAY_BUFFER: u32 = 0x8893;
    pub const GL_ELEMENT_ARRAY_BUFFER_BINDING: u32 = 0x8895;
    pub const GL_EQUAL: u32 = 0x0202;
    pub const GL_EXTENSIONS: u32 = 0x1F03;
    pub const GL_FALSE: u8 = 0;
    pub const GL_FASTEST: u32 = 0x1101;
    pub const GL_FIXED: u32 = 0x140C;
    pub const GL_FLOAT: u32 = 0x1406;
    pub const GL_FLOAT_MAT2: u32 = 0x8B5A;
    pub const GL_FLOAT_MAT3: u32 = 0x8B5B;
    pub const GL_FLOAT_MAT4: u32 = 0x8B5C;
    pub const GL_FLOAT_VEC2: u32 = 0x8B50;
    pub const GL_FLOAT_VEC3: u32 = 0x8B51;
    pub const GL_FLOAT_VEC4: u32 = 0x8B52;
    pub const GL_FRAGMENT_SHADER: u32 = 0x8B30;
    pub const GL_FRAMEBUFFER: u32 = 0x8D40;
    pub const GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME: u32 = 0x8CD1;
    pub const GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE: u32 = 0x8CD0;
    pub const GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE: u32 = 0x8CD3;
    pub const GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL: u32 = 0x8CD2;
    pub const GL_FRAMEBUFFER_BINDING: u32 = 0x8CA6;
    pub const GL_FRAMEBUFFER_COMPLETE: u32 = 0x8CD5;
    pub const GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: u32 = 0x8CD6;
    pub const GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS: u32 = 0x8CD9;
    pub const GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: u32 = 0x8CD7;
    pub const GL_FRAMEBUFFER_UNSUPPORTED: u32 = 0x8CDD;
    pub const GL_FRONT: u32 = 0x0404;
    pub const GL_FRONT_AND_BACK: u32 = 0x0408;
    pub const GL_FRONT_FACE: u32 = 0x0B46;
    pub const GL_FUNC_ADD: u32 = 0x8006;
    pub const GL_FUNC_REVERSE_SUBTRACT: u32 = 0x800B;
    pub const GL_FUNC_SUBTRACT: u32 = 0x800A;
    pub const GL_GENERATE_MIPMAP_HINT: u32 = 0x8192;
    pub const GL_GEQUAL: u32 = 0x0206;
    pub const GL_GREATER: u32 = 0x0204;
    pub const GL_GREEN_BITS: u32 = 0x0D53;
    pub const GL_HIGH_FLOAT: u32 = 0x8DF2;
    pub const GL_HIGH_INT: u32 = 0x8DF5;
    pub const GL_IMPLEMENTATION_COLOR_READ_FORMAT: u32 = 0x8B9B;
    pub const GL_IMPLEMENTATION_COLOR_READ_TYPE: u32 = 0x8B9A;
    pub const GL_INCR: u32 = 0x1E02;
    pub const GL_INCR_WRAP: u32 = 0x8507;
    pub const GL_INFO_LOG_LENGTH: u32 = 0x8B84;
    pub const GL_INT: u32 = 0x1404;
    pub const GL_INT_VEC2: u32 = 0x8B53;
    pub const GL_INT_VEC3: u32 = 0x8B54;
    pub const GL_INT_VEC4: u32 = 0x8B55;
    pub const GL_INVALID_ENUM: u32 = 0x0500;
    pub const GL_INVALID_FRAMEBUFFER_OPERATION: u32 = 0x0506;
    pub const GL_INVALID_OPERATION: u32 = 0x0502;
    pub const GL_INVALID_VALUE: u32 = 0x0501;
    pub const GL_INVERT: u32 = 0x150A;
    pub const GL_KEEP: u32 = 0x1E00;
    pub const GL_LEQUAL: u32 = 0x0203;
    pub const GL_LESS: u32 = 0x0201;
    pub const GL_LINEAR: u32 = 0x2601;
    pub const GL_LINEAR_MIPMAP_LINEAR: u32 = 0x2703;
    pub const GL_LINEAR_MIPMAP_NEAREST: u32 = 0x2701;
    pub const GL_LINES: u32 = 0x0001;
    pub const GL_LINE_LOOP: u32 = 0x0002;
    pub const GL_LINE_STRIP: u32 = 0x0003;
    pub const GL_LINE_WIDTH: u32 = 0x0B21;
    pub const GL_LINK_STATUS: u32 = 0x8B82;
    pub const GL_LOW_FLOAT: u32 = 0x8DF0;
    pub const GL_LOW_INT: u32 = 0x8DF3;
    pub const GL_LUMINANCE: u32 = 0x1909;
    pub const GL_LUMINANCE_ALPHA: u32 = 0x190A;
    pub const GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: u32 = 0x8B4D;
    pub const GL_MAX_CUBE_MAP_TEXTURE_SIZE: u32 = 0x851C;
    pub const GL_MAX_DEBUG_GROUP_STACK_DEPTH_KHR: u32 = 0x826C;
    pub const GL_MAX_DEBUG_LOGGED_MESSAGES_KHR: u32 = 0x9144;
    pub const GL_MAX_DEBUG_MESSAGE_LENGTH_KHR: u32 = 0x9143;
    pub const GL_MAX_FRAGMENT_UNIFORM_VECTORS: u32 = 0x8DFD;
    pub const GL_MAX_LABEL_LENGTH_KHR: u32 = 0x82E8;
    pub const GL_MAX_RENDERBUFFER_SIZE: u32 = 0x84E8;
    pub const GL_MAX_TEXTURE_IMAGE_UNITS: u32 = 0x8872;
    pub const GL_MAX_TEXTURE_SIZE: u32 = 0x0D33;
    pub const GL_MAX_VARYING_VECTORS: u32 = 0x8DFC;
    pub const GL_MAX_VERTEX_ATTRIBS: u32 = 0x8869;
    pub const GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS: u32 = 0x8B4C;
    pub const GL_MAX_VERTEX_UNIFORM_VECTORS: u32 = 0x8DFB;
    pub const GL_MAX_VIEWPORT_DIMS: u32 = 0x0D3A;
    pub const GL_MEDIUM_FLOAT: u32 = 0x8DF1;
    pub const GL_MEDIUM_INT: u32 = 0x8DF4;
    pub const GL_MIRRORED_REPEAT: u32 = 0x8370;
    pub const GL_NEAREST: u32 = 0x2600;
    pub const GL_NEAREST_MIPMAP_LINEAR: u32 = 0x2702;
    pub const GL_NEAREST_MIPMAP_NEAREST: u32 = 0x2700;
    pub const GL_NEVER: u32 = 0x0200;
    pub const GL_NICEST: u32 = 0x1102;
    pub const GL_NONE: u32 = 0;
    pub const GL_NOTEQUAL: u32 = 0x0205;
    pub const GL_NO_ERROR: u32 = 0;
    pub const GL_NUM_COMPRESSED_TEXTURE_FORMATS: u32 = 0x86A2;
    pub const GL_NUM_SHADER_BINARY_FORMATS: u32 = 0x8DF9;
    pub const GL_ONE: u32 = 1;
    pub const GL_ONE_MINUS_CONSTANT_ALPHA: u32 = 0x8004;
    pub const GL_ONE_MINUS_CONSTANT_COLOR: u32 = 0x8002;
    pub const GL_ONE_MINUS_DST_ALPHA: u32 = 0x0305;
    pub const GL_ONE_MINUS_DST_COLOR: u32 = 0x0307;
    pub const GL_ONE_MINUS_SRC_ALPHA: u32 = 0x0303;
    pub const GL_ONE_MINUS_SRC_COLOR: u32 = 0x0301;
    pub const GL_OUT_OF_MEMORY: u32 = 0x0505;
    pub const GL_PACK_ALIGNMENT: u32 = 0x0D05;
    pub const GL_POINTS: u32 = 0x0000;
    pub const GL_POLYGON_OFFSET_FACTOR: u32 = 0x8038;
    pub const GL_POLYGON_OFFSET_FILL: u32 = 0x8037;
    pub const GL_POLYGON_OFFSET_UNITS: u32 = 0x2A00;
    pub const GL_PROGRAM_KHR: u32 = 0x82E2;
    pub const GL_PROGRAM_PIPELINE_KHR: u32 = 0x82E4;
    pub const GL_QUERY_KHR: u32 = 0x82E3;
    pub const GL_RED_BITS: u32 = 0x0D52;
    pub const GL_RENDERBUFFER: u32 = 0x8D41;
    pub const GL_RENDERBUFFER_ALPHA_SIZE: u32 = 0x8D53;
    pub const GL_RENDERBUFFER_BINDING: u32 = 0x8CA7;
    pub const GL_RENDERBUFFER_BLUE_SIZE: u32 = 0x8D52;
    pub const GL_RENDERBUFFER_DEPTH_SIZE: u32 = 0x8D54;
    pub const GL_RENDERBUFFER_GREEN_SIZE: u32 = 0x8D51;
    pub const GL_RENDERBUFFER_HEIGHT: u32 = 0x8D43;
    pub const GL_RENDERBUFFER_INTERNAL_FORMAT: u32 = 0x8D44;
    pub const GL_RENDERBUFFER_RED_SIZE: u32 = 0x8D50;
    pub const GL_RENDERBUFFER_STENCIL_SIZE: u32 = 0x8D55;
    pub const GL_RENDERBUFFER_WIDTH: u32 = 0x8D42;
    pub const GL_RENDERER: u32 = 0x1F01;
    pub const GL_REPEAT: u32 = 0x2901;
    pub const GL_REPLACE: u32 = 0x1E01;
    pub const GL_RGB: u32 = 0x1907;
    pub const GL_RGB565: u32 = 0x8D62;
    pub const GL_RGB5_A1: u32 = 0x8057;
    pub const GL_RGBA: u32 = 0x1908;
    pub const GL_RGBA4: u32 = 0x8056;
    pub const GL_SAMPLER_2D: u32 = 0x8B5E;
    pub const GL_SAMPLER_CUBE: u32 = 0x8B60;
    pub const GL_SAMPLER_KHR: u32 = 0x82E6;
    pub const GL_SAMPLES: u32 = 0x80A9;
    pub const GL_SAMPLE_ALPHA_TO_COVERAGE: u32 = 0x809E;
    pub const GL_SAMPLE_BUFFERS: u32 = 0x80A8;
    pub const GL_SAMPLE_COVERAGE: u32 = 0x80A0;
    pub const GL_SAMPLE_COVERAGE_INVERT: u32 = 0x80AB;
    pub const GL_SAMPLE_COVERAGE_VALUE: u32 = 0x80AA;
    pub const GL_SCISSOR_BOX: u32 = 0x0C10;
    pub const GL_SCISSOR_TEST: u32 = 0x0C11;
    pub const GL_SHADER_BINARY_FORMATS: u32 = 0x8DF8;
    pub const GL_SHADER_COMPILER: u32 = 0x8DFA;
    pub const GL_SHADER_KHR: u32 = 0x82E1;
    pub const GL_SHADER_SOURCE_LENGTH: u32 = 0x8B88;
    pub const GL_SHADER_TYPE: u32 = 0x8B4F;
    pub const GL_SHADING_LANGUAGE_VERSION: u32 = 0x8B8C;
    pub const GL_SHORT: u32 = 0x1402;
    pub const GL_SRC_ALPHA: u32 = 0x0302;
    pub const GL_SRC_ALPHA_SATURATE: u32 = 0x0308;
    pub const GL_SRC_COLOR: u32 = 0x0300;
    pub const GL_STACK_OVERFLOW_KHR: u32 = 0x0503;
    pub const GL_STACK_UNDERFLOW_KHR: u32 = 0x0504;
    pub const GL_STATIC_DRAW: u32 = 0x88E4;
    pub const GL_STENCIL_ATTACHMENT: u32 = 0x8D20;
    pub const GL_STENCIL_BACK_FAIL: u32 = 0x8801;
    pub const GL_STENCIL_BACK_FUNC: u32 = 0x8800;
    pub const GL_STENCIL_BACK_PASS_DEPTH_FAIL: u32 = 0x8802;
    pub const GL_STENCIL_BACK_PASS_DEPTH_PASS: u32 = 0x8803;
    pub const GL_STENCIL_BACK_REF: u32 = 0x8CA3;
    pub const GL_STENCIL_BACK_VALUE_MASK: u32 = 0x8CA4;
    pub const GL_STENCIL_BACK_WRITEMASK: u32 = 0x8CA5;
    pub const GL_STENCIL_BITS: u32 = 0x0D57;
    pub const GL_STENCIL_BUFFER_BIT: u32 = 0x00000400;
    pub const GL_STENCIL_CLEAR_VALUE: u32 = 0x0B91;
    pub const GL_STENCIL_FAIL: u32 = 0x0B94;
    pub const GL_STENCIL_FUNC: u32 = 0x0B92;
    pub const GL_STENCIL_INDEX8: u32 = 0x8D48;
    pub const GL_STENCIL_PASS_DEPTH_FAIL: u32 = 0x0B95;
    pub const GL_STENCIL_PASS_DEPTH_PASS: u32 = 0x0B96;
    pub const GL_STENCIL_REF: u32 = 0x0B97;
    pub const GL_STENCIL_TEST: u32 = 0x0B90;
    pub const GL_STENCIL_VALUE_MASK: u32 = 0x0B93;
    pub const GL_STENCIL_WRITEMASK: u32 = 0x0B98;
    pub const GL_STREAM_DRAW: u32 = 0x88E0;
    pub const GL_SUBPIXEL_BITS: u32 = 0x0D50;
    pub const GL_TEXTURE: u32 = 0x1702;
    pub const GL_TEXTURE0: u32 = 0x84C0;
    pub const GL_TEXTURE1: u32 = 0x84C1;
    pub const GL_TEXTURE10: u32 = 0x84CA;
    pub const GL_TEXTURE11: u32 = 0x84CB;
    pub const GL_TEXTURE12: u32 = 0x84CC;
    pub const GL_TEXTURE13: u32 = 0x84CD;
    pub const GL_TEXTURE14: u32 = 0x84CE;
    pub const GL_TEXTURE15: u32 = 0x84CF;
    pub const GL_TEXTURE16: u32 = 0x84D0;
    pub const GL_TEXTURE17: u32 = 0x84D1;
    pub const GL_TEXTURE18: u32 = 0x84D2;
    pub const GL_TEXTURE19: u32 = 0x84D3;
    pub const GL_TEXTURE2: u32 = 0x84C2;
    pub const GL_TEXTURE20: u32 = 0x84D4;
    pub const GL_TEXTURE21: u32 = 0x84D5;
    pub const GL_TEXTURE22: u32 = 0x84D6;
    pub const GL_TEXTURE23: u32 = 0x84D7;
    pub const GL_TEXTURE24: u32 = 0x84D8;
    pub const GL_TEXTURE25: u32 = 0x84D9;
    pub const GL_TEXTURE26: u32 = 0x84DA;
    pub const GL_TEXTURE27: u32 = 0x84DB;
    pub const GL_TEXTURE28: u32 = 0x84DC;
    pub const GL_TEXTURE29: u32 = 0x84DD;
    pub const GL_TEXTURE3: u32 = 0x84C3;
    pub const GL_TEXTURE30: u32 = 0x84DE;
    pub const GL_TEXTURE31: u32 = 0x84DF;
    pub const GL_TEXTURE4: u32 = 0x84C4;
    pub const GL_TEXTURE5: u32 = 0x84C5;
    pub const GL_TEXTURE6: u32 = 0x84C6;
    pub const GL_TEXTURE7: u32 = 0x84C7;
    pub const GL_TEXTURE8: u32 = 0x84C8;
    pub const GL_TEXTURE9: u32 = 0x84C9;
    pub const GL_TEXTURE_2D: u32 = 0x0DE1;
    pub const GL_TEXTURE_BINDING_2D: u32 = 0x8069;
    pub const GL_TEXTURE_BINDING_CUBE_MAP: u32 = 0x8514;
    pub const GL_TEXTURE_CUBE_MAP: u32 = 0x8513;
    pub const GL_TEXTURE_CUBE_MAP_NEGATIVE_X: u32 = 0x8516;
    pub const GL_TEXTURE_CUBE_MAP_NEGATIVE_Y: u32 = 0x8518;
    pub const GL_TEXTURE_CUBE_MAP_NEGATIVE_Z: u32 = 0x851A;
    pub const GL_TEXTURE_CUBE_MAP_POSITIVE_X: u32 = 0x8515;
    pub const GL_TEXTURE_CUBE_MAP_POSITIVE_Y: u32 = 0x8517;
    pub const GL_TEXTURE_CUBE_MAP_POSITIVE_Z: u32 = 0x8519;
    pub const GL_TEXTURE_MAG_FILTER: u32 = 0x2800;
    pub const GL_TEXTURE_MIN_FILTER: u32 = 0x2801;
    pub const GL_TEXTURE_WRAP_S: u32 = 0x2802;
    pub const GL_TEXTURE_WRAP_T: u32 = 0x2803;
    pub const GL_TRIANGLES: u32 = 0x0004;
    pub const GL_TRIANGLE_FAN: u32 = 0x0006;
    pub const GL_TRIANGLE_STRIP: u32 = 0x0005;
    pub const GL_TRUE: u8 = 1;
    pub const GL_UNPACK_ALIGNMENT: u32 = 0x0CF5;
    pub const GL_UNSIGNED_BYTE: u32 = 0x1401;
    pub const GL_UNSIGNED_INT: u32 = 0x1405;
    pub const GL_UNSIGNED_SHORT: u32 = 0x1403;
    pub const GL_UNSIGNED_SHORT_4_4_4_4: u32 = 0x8033;
    pub const GL_UNSIGNED_SHORT_5_5_5_1: u32 = 0x8034;
    pub const GL_UNSIGNED_SHORT_5_6_5: u32 = 0x8363;
    pub const GL_VALIDATE_STATUS: u32 = 0x8B83;
    pub const GL_VENDOR: u32 = 0x1F00;
    pub const GL_VERSION: u32 = 0x1F02;
    pub const GL_VERTEX_ARRAY_BINDING_OES: u32 = 0x85B5;
    pub const GL_VERTEX_ARRAY_KHR: u32 = 0x8074;
    pub const GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING: u32 = 0x889F;
    pub const GL_VERTEX_ATTRIB_ARRAY_ENABLED: u32 = 0x8622;
    pub const GL_VERTEX_ATTRIB_ARRAY_NORMALIZED: u32 = 0x886A;
    pub const GL_VERTEX_ATTRIB_ARRAY_POINTER: u32 = 0x8645;
    pub const GL_VERTEX_ATTRIB_ARRAY_SIZE: u32 = 0x8623;
    pub const GL_VERTEX_ATTRIB_ARRAY_STRIDE: u32 = 0x8624;
    pub const GL_VERTEX_ATTRIB_ARRAY_TYPE: u32 = 0x8625;
    pub const GL_VERTEX_SHADER: u32 = 0x8B31;
    pub const GL_VIEWPORT: u32 = 0x0BA2;
    pub const GL_ZERO: u32 = 0;
}
