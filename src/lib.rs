// Increase the recursion limit for error-chain
#![recursion_limit = "512"]

#[macro_use]
extern crate error_chain;

#[cfg(target_arch = "wasm32")]
#[macro_use]
extern crate stdweb;

#[cfg(target_arch = "wasm32")]
#[path = "webgl.rs"]
pub mod webgl;

#[cfg(all(not(target_arch = "wasm32"), not(feature = "desktop-gl")))]
extern crate glad_gles2;

#[cfg(not(target_arch = "wasm32"))]
#[path = "webgl_gles.rs"]
mod webgl;

pub const IS_GL_ES: bool = cfg!(not(target_arch = "wasm32"));

pub mod glenum;
pub mod errors;
pub mod api;
mod helper;

pub mod common {

    use std::ops::{Deref, DerefMut};
    use std::fmt;
    use super::webgl::Reference;
    use super::webgl::GLContext;

    pub struct WebGLRenderingContext {
        pub ctx: GLContext,
    }

    impl fmt::Debug for WebGLRenderingContext {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            f.write_str("WebGLRenderingContext { ... }")
        }
    }

    impl Deref for WebGLRenderingContext {
        type Target = GLContext;
        #[inline]
        fn deref(&self) -> &Self::Target {
            &self.ctx
        }
    }
    impl DerefMut for WebGLRenderingContext {
        #[inline]
        fn deref_mut(&mut self) -> &mut Self::Target {
            &mut self.ctx
        }
    }
    impl AsRef<GLContext> for WebGLRenderingContext {
        #[inline]
        fn as_ref(&self) -> &GLContext { &self.ctx }
    }
    impl AsMut<GLContext> for WebGLRenderingContext {
        #[inline]
        fn as_mut(&mut self) -> &mut GLContext { &mut self.ctx }
    }

    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WebGLBuffer(pub Reference);
    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WebGLTexture(pub Reference);
    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WebGLShader(pub Reference);
    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WebGLProgram(pub Reference);
    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WebGLVertexArray(pub Reference);
    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WebGLFramebuffer(pub Reference);
    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WebGLRenderbuffer(pub Reference);
    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WebGLUniformLocation {
        pub reference: Reference,
        // pub name: String,
    }
}

pub use common::*;
pub use webgl::*;
pub use api::{IGLContext, WebGLActiveInfo, WebGLParameterOut};
pub mod gle {
    pub use super::glenum::*;
    pub use super::glenum::enumerations::*;
}
