use super::common::*;
use super::glenum::*;
use super::helper::*;
use super::errors::{Error};
use crate::api::IGLContext;
use stdweb::UnsafeTypedArray;
use stdweb::web::*;
use stdweb::unstable::TryInto;

pub type Reference = i32;

pub struct GLContext {
    pub reference: Reference,
    pub is_webgl2: bool,
}

macro_rules! uniform_impl {
    ( $me:ident; $loc:ident; $real_name:ident; $error_name:expr; $($decomp:tt)*) => {
        $me.log($error_name);
        js! {
            @(no_return)
            var ctx = Module.gl.get(@{$me.reference});
            var loc = Module.gl.get(@{$loc.reference});
            ctx. $real_name ( loc, $($decomp)* );
        }
    };
}

macro_rules! create_impl {
    ( $me:ident; $func_name:ident; $error_name:expr; $($arg:expr),* ) => {
        {
            $me.log($error_name);
            let value = js!({
                var ctx = Module.gl.get(@{$me.reference});
                return Module.gl.add(ctx. $func_name ( $($arg),* ));
            });
            let r: i32 = value.try_into().unwrap();
            r
        }
    };
}

macro_rules! delete_impl {
    ( $me:ident; $func_name:ident; $error_name:expr; $obj:ident ) => {
        {
            $me.log($error_name);
            js! { @(no_return)
                var ctx = Module.gl.get(@{$me.reference});
                var b = Module.gl.get(@{$obj.0});
                ctx. $func_name (b);
                Module.gl.remove(b);
            };
            $me.check_gl_error($error_name);
        }
    };
}

macro_rules! no_return_impl {
    ( $me:ident; $func_name:ident; $error_name:expr; $($arg:expr),* ) => {
        $me.log($error_name);
        js! { @(no_return)
            var ctx = Module.gl.get(@{$me.reference});
            ctx. $func_name ( $(@{ $arg }),* );
        };
        $me.check_gl_error($error_name);
    };
}

impl GLContext {

    #[inline]
    pub fn log<T: Into<String>>(&self, _msg: T) {
        // js!{ console.log(@{_msg.into()})};
    }

    pub fn new<'a>(canvas: &Element) -> Result<GLContext, Error> {
        let gl = js! {
            var gl = (@{canvas}).getContext("webgl2", {alpha:false, preserveDrawingBuffer:true});
            var version = 2;

            if (!gl) {
                gl = (@{canvas}).getContext("webgl", {alpha:false, preserveDrawingBuffer:true});
                var extensions = [
                    {
                        name: "OES_vertex_array_object",
                        required: true,
                        fields: {
                            "createVertexArray": "createVertexArrayOES",
                            "deleteVertexArray": "deleteVertexArrayOES",
                            "bindVertexArray":   "bindVertexArrayOES",
                        } ,
                    },
                    {
                        name: "OES_element_index_uint",
                        required: false,
                        fields: {},
                    },
                ];
                // For each extension copy the values that we need, to keep compatibility
                // between webgl 1 and 2.
                extensions.forEach(ext_info => {
                    var ext = gl.getExtension(ext_info.name);
                    if (!ext) {
                        if (ext_info.required) {
                            throw "Can't load extension";
                        }
                    } else {
                        Object.keys(ext_info.fields).forEach(fk => {
                            gl[fk] = ext[ext_info.fields[fk]];
                        });
                    }
                });
                version = 1;
            }

            // Create gl related objects
            if( !Module.gl) {
                Module.gl = {};
                Module.gl.counter = 1;
                Module.gl.version = version;

                Module.gl.matrix4x4 = new Float32Array([
                    1.0, 0,   0,   0,
                    0,   1.0, 0.0, 0,
                    0,   0,   1.0, 0,
                    0,   0,   0,   1.0
                ]);

                Module.gl.pool = {};
                Module.gl.get = function(id) {
                    return Module.gl.pool[id];
                };
                Module.gl.add = function(o) {
                    var c = Module.gl.counter;
                    Module.gl.pool[c] = o;
                    Module.gl.counter += 1;
                    return c;
                };

                Module.gl.remove = function(id) {
                    delete Module.gl.pool[id];
                    return c;
                };
                console.log("opengl "+gl.getParameter(gl.VERSION));
                console.log("shading language " + gl.getParameter(gl.SHADING_LANGUAGE_VERSION));
                console.log("vendor " + gl.getParameter(gl.VENDOR));
            }

            return Module.gl.add(gl);
        };

        let version: u32 = js!( return Module.gl.version; ).try_into().unwrap();

        Ok(GLContext {
            reference: gl.try_into().unwrap(),
            is_webgl2: version == 2,
        })
    }

    // We force-inline this to ensure that if "check-all-errors" is off it won't generate any code
    #[inline]
    fn check_gl_error(&self, msg: &str) {
        if cfg!(feature = "check-all-errors") {
            self.panic_gl_error(msg);
        }
    }

    fn panic_gl_error(&self, msg: &str) {
        let err = self.get_error();
        if err != ErrorCode::NoError as u32 {
            let emsg = match ErrorCode::from_u32(err) {
                Some(c) => c.as_str(),
                None => "unknown error",
            };
            panic!(format!("GLError: {} {} ({})",  msg, err, emsg));
        }
    }
}

impl IGLContext for GLContext {

    // A
    fn active_texture(&self, unit: u32) {
        let value = GL_TEXTURE0 + unit;
        no_return_impl!( self; activeTexture; "active_texture"; value );
    }

    /// attach a shader to a program. A program must have two shaders : vertex and fragment shader.
    fn attach_shader(&self, program: &WebGLProgram, shader: &WebGLShader) {
        self.log("attach_shader");
        js! {
            @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var p = Module.gl.get(@{program.0});
            var s = Module.gl.get(@{shader.0});
            ctx.attachShader(p, s);
        };
        self.check_gl_error("attach_shader");
    }

    // B

    /// associate a generic vertex attribute index with a named attribute
    fn bind_attrib_location(&self, program: &WebGLProgram, loc: u32, name: &str) {
        self.log("bind_attrib_location");
        js!{ @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var h = Module.gl.get(@{program.0});
            ctx.bindAttribLocation(h.prog, @{loc}, @{name});
        };
        self.check_gl_error("bind_attrib_location");
    }

    fn bind_buffer(&self, kind: BufferKind, buffer_o: Option<&WebGLBuffer>) {
        self.log("bind_buffer");
        if let Some(buffer) = buffer_o {
            js!{ @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                var b = Module.gl.get(@{buffer.0});
                ctx.bindBuffer(@{kind as u32}, b);
            };
        } else {
            js! { @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                ctx.bindBuffer(@{kind as u32}, null);
            }
        }
        self.check_gl_error("bind_buffer");
    }

    /// bind a framebuffer to the current state
    fn bind_framebuffer(&self, fb_o: Option<&WebGLFramebuffer>) {
        self.log("bind_framebuffer");
        if let Some(fb) = fb_o {
            js!{ @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                var fb = Module.gl.get(@{fb.0});
                ctx.bindFramebuffer(ctx.FRAMEBUFFER, fb);
            };
        } else {
            js!{ @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                ctx.bindFramebuffer(ctx.FRAMEBUFFER, null);
            };
        }
        self.check_gl_error("bind_framebuffer");
    }

    fn bind_renderbuffer(&self, rb_o: Option<&WebGLRenderbuffer>) {
        self.log("bind_renderbuffer");
        if let Some(rb) = rb_o {
            js!{ @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                var rb = Module.gl.get(@{rb.0});
                ctx.bindRenderbuffer(ctx.RENDERBUFFER, rb);
            };
        } else {
            js!{ @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                ctx.bindRenderbuffer(ctx.RENDERBUFFER, null);
            };
        }
        self.check_gl_error("bind_renderbuffer");
    }

    fn bind_texture(&self, target: TextureTarget, tex_o: Option<&WebGLTexture>) {
        self.log("bind_texture");
        if let Some(tex) = tex_o {
            js!{ @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                var b = Module.gl.get(@{tex.0});
                ctx.bindTexture(@{target as u32}, b);
            };
        } else {
            js!{ @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                ctx.bindTexture(@{target as u32}, null);
            };
        }
        self.check_gl_error("bind_texture");
    }

    /// bind a vertex array object to current state
    fn bind_vertex_array(&self, vao_o: Option<&WebGLVertexArray>) {
        self.log("bind_vertex_array");
        if let Some(vao) = vao_o {
            js! {
                @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                var b = Module.gl.get(@{vao.0});
                ctx.bindVertexArray(b);
            };
        } else {
            js!{ @(no_return)
                var ctx = Module.gl.get(@{self.reference});
                ctx.bindVertexArray(null);
            };
        }
        self.check_gl_error("bind_vertex_array");
    }

    fn blend_color(&self, r: f32, g: f32, b: f32, a: f32) {
        no_return_impl!( self; blendColor; "blend_color"; r, g, b, a );
    }

    fn blend_equation(&self, mode: BlendMode) {
        self.blend_equation_separate(mode, mode)
    }

    fn blend_equation_separate(&self, mode_rgb: BlendMode, mode_alpha: BlendMode) {
        no_return_impl!( self; blendEquationSeparate; "blend_equation_separate";
            mode_rgb as u32, mode_alpha as u32);
    }

    fn blend_func(&self, sfactor: BlendFactor, dfactor: BlendFactor) {
        no_return_impl!( self; blendFunc; "blend_func";
            sfactor as u32, dfactor as u32);
    }

    fn blend_func_separate(&self, src_rgb: BlendFactor, dst_rgb: BlendFactor,
                            src_a: BlendFactor, dst_a: BlendFactor
    ) {
        no_return_impl!( self; blendFuncSeparate; "blend_func_separate";
            src_rgb as u32, dst_rgb as u32, src_a as u32, dst_a as u32);
    }

    fn buffer_data<T: Sized>(&self, kind: BufferKind, data: &[T], usage: BufferUsage) {
        self.log("buffer_data");
        let u8d = slice_as_u8(data);
        js! {
            @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            ctx.bufferData(@{kind as u32},@{ TypedArray::from(u8d) }, @{usage as u32})
        };
        self.check_gl_error("buffer_data");
    }

    fn buffer_sub_data<T: Sized>(&self, kind: BufferKind, offset: u32, data: &[T]) {
        self.log("buffer_sub_data");
        let u8d = slice_as_u8(data);
        js! {
            @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            ctx.bufferSubData(@{kind as u32}, @{offset}, @{ TypedArray::from(u8d) });
        };
        self.check_gl_error("buffer_sub_data");
    }

    // C

    fn check_framebuffer_status(&self) -> FramebufferError {
        let value = js!{
            var ctx = Module.gl.get(@{self.reference});
            return ctx.checkFramebufferStatus(ctx.FRAMEBUFFER);
        };
        let r: u32 = value.try_into().unwrap();
        FramebufferError::from_u32(r)
    }

    fn clear(&self, mask: u32) {
        no_return_impl!( self; clear; "clear"; mask );
    }

    /// specify clear values for the color buffers
    fn clear_color(&self, r: f32, g: f32, b: f32, a: f32) {
        self.log("clear_color");
        js! {
            @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            ctx.clearColor(@{r}, @{g}, @{b}, @{a});
        };
        self.check_gl_error("clear_color");
    }

    fn clear_depth(&self, depth: f32) {
        no_return_impl!( self; clearDepth; "clear_depth"; depth );
    }

    fn clear_stencil(&self, s: i32) {
        no_return_impl!( self; clearStencil; "clear_stencil"; s );
    }

    fn color_mask(&self, r: bool, g: bool, b: bool, a: bool) {
        no_return_impl!( self; colorMask; "color_mask";
            r, g, b, a );
    }

    fn compile_shader(&self, shader: &WebGLShader) -> Result<(), String> {
        self.log("compile_shader");
        let value = js! {
            var ctx = Module.gl.get(@{&self.reference});
            var shader = Module.gl.get(@{shader.0});
            ctx.compileShader(shader);

            var compiled = ctx.getShaderParameter(shader, 0x8B81);
            if (!compiled ) {
                return ctx.getShaderInfoLog(shader);
            } else {
                return null;
            }
        };
        self.check_gl_error("compile_shader");
        if value.is_null() {
            Ok(())
        } else {
            Err(value.as_str().unwrap().into())
        }
    }

    fn copy_tex_image2d(&self, target: TextureTarget, level: u8, format: PixelFormat,
                        x: u32, y: u32, width: u32, height: u32
    ) {
        no_return_impl!( self; copyTexImage2D; "copy_tex_image2d";
            target as u32, level as u32, format as u32, x, y, width, height, 0);
    }

    // TODO pub fn copy_tex_sub_image2d() {}

    fn create_buffer(&self) -> WebGLBuffer {
        let value = create_impl!( self; createBuffer; "create_buffer"; );
        self.check_gl_error("create_buffer");
        WebGLBuffer(value)
    }

    /// create a new framebuffer
    fn create_framebuffer(&self) -> WebGLFramebuffer {
        let value = create_impl!( self; createFramebuffer; "create_framebuffer"; );
        self.check_gl_error("create_framebuffer");
        WebGLFramebuffer(value)
    }

    fn create_program(&self) -> WebGLProgram {
        let value = create_impl!( self; createProgram; "create_program"; );
        self.check_gl_error("create_program");
        WebGLProgram(value)
    }

    fn create_shader(&self, kind: ShaderKind) -> WebGLShader {
        let _ = kind;
        let value = create_impl!( self; createShader; "create_shader"; kind as u32 );
        self.check_gl_error("create_shader");
        WebGLShader(value)
    }

    fn create_renderbuffer(&self) -> WebGLRenderbuffer {
        let value = create_impl!( self; createRenderbuffer; "create_renderbuffer"; );
        self.check_gl_error("create_renderbuffer");
        WebGLRenderbuffer(value)
    }

    fn create_texture(&self) -> WebGLTexture {
        let value = create_impl!( self; createTexture; "create_texture"; );
        self.check_gl_error("create_texture");
        WebGLTexture(value)
    }

    /// create a vertex array object
    fn create_vertex_array(&self) -> WebGLVertexArray {
        let value = create_impl!( self; createVertexArray; "create_vertex_array"; );
        self.check_gl_error("create_vertex_array");
        WebGLVertexArray(value)
    }

    fn cull_face(&self, mode: FaceMode) {
        self.log("cull_face");
        js! { @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            ctx.cullFace(@{mode as u32});
        };
        self.check_gl_error("cull_face");
    }

    // D

    fn delete_buffer(&self, buffer: &WebGLBuffer) {
        delete_impl!( self; deleteBuffer; "delete_buffer"; buffer )
    }

    /// destroy a framebuffer
    fn delete_framebuffer(&self, fb: &WebGLFramebuffer) {
        delete_impl!( self; deleteFramebuffer; "delete_framebuffer"; fb )
    }

    fn delete_program(&self, program: &WebGLProgram) {
        delete_impl!( self; deleteProgram; "delete_program"; program )
    }

    fn delete_renderbuffer(&self, rb: &WebGLRenderbuffer) {
        delete_impl!( self; deleteRenderbuffer; "delete_renderbuffer"; rb )
    }

    fn delete_shader(&self, shader: &WebGLShader) {
        delete_impl!( self; deleteShader; "delete_shader"; shader )
    }

    fn delete_texture(&self, tex: &WebGLTexture) {
        delete_impl!( self; deleteTexture; "delete_texture"; tex )
    }

    /// destroy a vertex array object
    fn delete_vertex_array(&self, vao: &WebGLVertexArray) {
        delete_impl!( self; deleteVertexArray; "delete_vertex_array"; vao )
    }

    fn depth_func(&self, func: CompareOp) {
        no_return_impl!( self; depthFunc; "depth_func"; func as u32 );
    }

    fn depth_mask(&self, flag: bool) {
        no_return_impl!( self; depthMask; "depth_mask"; flag as u32 );
    }

    fn depth_range(&self, z_near: f32, z_far: f32) {
        no_return_impl!( self; depthRange; "depth_range"; z_near, z_far );
    }

    fn detach_shader(&self, program: &WebGLProgram, shader: &WebGLShader) {
        self.log("deatch_shader");
        js!{ @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var p = Module.gl.get(@{program.0});
            var s = Module.gl.get(@{shader.0});
            ctx.detachShader(p, s);
        };
        self.check_gl_error("detach_shader");
    }

    /// disable GL capabilities.
    ///
    /// flag should be one of [`Flag`]
    fn disable(&self, flag: Flag) {
        no_return_impl!( self; disable; "disable"; flag as u32 );
    }

    fn disable_vertex_attrib_array(&self, loc: u32) {
        no_return_impl!( self; disableVertexAttribArray; "disable_vertex_attrib_array"; loc );
    }
    // TODO pub fn disable_vertex_attrib_array()

    /// render primitives from array data
    fn draw_arrays(&self, mode: Primitives, first: u32, count: u32) {
        no_return_impl!( self; drawArrays; "draw_arrays"; mode as u32, first, count);
    }

    /// render primitives from indexed array data
    fn draw_elements(&self, mode: Primitives, count: usize, kind: DataType, offset: u32) {
        no_return_impl!( self; drawElements; "draw_elements";
            mode as u32, count as u32, kind as u32, offset);
    }

    // E

    /// enable GL capabilities.
    ///
    /// flag should be one of [`Flag`]
    fn enable(&self, flag: Flag) {
        no_return_impl!( self; enable; "enable"; flag );
    }

    /// enable a generic vertex attribute array
    fn enable_vertex_attrib_array(&self, location: u32) {
        no_return_impl!( self; enableVertexAttribArray; "enable_vertex_attrib_array"; location );
    }

    // F

    fn finish(&self) {
        no_return_impl!( self; finish; "finish"; );
    }

    fn flush(&self) {
        no_return_impl!( self; flush; "flush"; );
    }

    fn framebuffer_renderbuffer(
        &self,
        attachment: Attachment,
        renderbuffer: &WebGLRenderbuffer
    ) {
        self.log("framebuffer_renderbuffer");
        js! { @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var rb = Module.gl.get(@{renderbuffer.0});
            ctx.framebufferRenderbuffer(ctx.FRAMEBUFFER, @{attachment as u32},
                ctx.RENDERBUFFER, rb);
        };
        self.check_gl_error("framebuffer_renderbuffer");
    }

    /// attach a texture to a framebuffer
    fn framebuffer_texture2d(
        &self,
        attachment: Attachment,
        textarget: TextureTarget,
        texture: &WebGLTexture,
        level: i32,
    ) {
        self.log("framebuffer_texture2d");
        js! { @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var rb = Module.gl.get(@{texture.0});
            ctx.framebufferTexture2D(ctx.FRAMEBUFFER, @{attachment as u32},
                @{textarget as u32}, rb, @{level});
        };
        self.check_gl_error("framebuffer_texture2d");
    }

    fn front_face(&self, mode: WindingMode) {
        no_return_impl!( self; frontFace; "front_face"; mode as u32 );
    }

    // G

    fn generate_mipmap(&self, target: TextureTarget) {
        no_return_impl!( self; generateMipmap; "generate_mipmap"; target as u32 );
    }

    fn get_active_attrib(program: &WebGLProgram, location: u32) -> Option<WebGLActiveInfo> {
        unimplemented!()
    }

    fn get_active_uniform(program: &WebGLProgram, index: u32) -> Option<WebGLActiveInfo> {
        unimplemented!()
    }

    /// return the location of an attribute variable
    fn get_attrib_location(&self, program: &WebGLProgram, name: &str) -> Option<u32> {
        self.log("get_attrib_location");
        let value = js! {
            var ctx = Module.gl.get(@{self.reference});
            var p = Module.gl.get(@{program.0});
            return ctx.getAttribLocation(p, @{name});
        };
        let r: i32 = value.try_into().unwrap();
        if r == -1 {
            None
        } else {
            Some(r as u32)
        }
    }

    fn get_error(&self) -> u32 {
        let code: u32 = js! {
            var ctx = Module.gl.get(@{self.reference});
            return ctx.getError(); 
        }.try_into().unwrap();
        code
    }

    fn get_parameter(parameter: u32) -> WebGLParameterOut {
        unimplemented!()
    }

    fn get_program_parameter(&self, program: &WebGLProgram, param: ProgramParameter) -> i32 {
        unimplemented!()
    }


    /// return the location of a uniform variable
    fn get_uniform_location(
        &self,
        program: &WebGLProgram,
        name: &str,
    ) -> Option<WebGLUniformLocation> {
        self.log("get_uniform_location");
        let value = js! {
            var ctx = Module.gl.get(@{self.reference});
            var p = Module.gl.get(@{program.0});
            var u = Module.gl.add(ctx.getUniformLocation(p, @{name}));
            return u;
        };
        let r: i32 = value.try_into().unwrap();
        if r == -1 {
            None
        } else {
            Some(WebGLUniformLocation {
                reference: r,
                // name: name.into(),
            })
        }
    }

    // L

    /// link a program
    fn link_program(&self, program: &WebGLProgram) -> Result<(), String> {
        self.log("link_program");
        let value = js! {
            var ctx = Module.gl.get(@{self.reference});
            var p = Module.gl.get(@{program.0});
            ctx.linkProgram(p);
            var r = ctx.getProgramParameter(p, ctx.LINK_STATUS);
            if (!r) {
                return ctx.getProgramInfoLog(p);
            } else {
                return null;
            }
        };
        self.check_gl_error("link_program");
        use stdweb::Value;
        match value {
            Value::Null => { Ok(()) },
            Value::String(s) => { Err(s) },
            _ => { Err("unknown error".into()) },
        }
    }

    // M

    // N

    // O

    // R

    fn read_pixels(&self, x: u32, y: u32, width: u32, height: u32,
                    format: PixelFormat, pixel_type: PixelType, data: &mut [u8]) {
        self.log("read_pixels");
        let bpp = bytes_per_pixel(format, pixel_type);
        if (bpp * width * height) > data.len() as u32 {
            panic!("Not enough storage space in buffer");
        }
        let array_kind = match pixel_type {
            PixelType::U8 => 1,
            PixelType::U16_5_6_5 | PixelType::U16_5_5_5_1 | PixelType::U16_4_4_4_4 => 2,
            PixelType::Float => 3,
        };
        let data_u8: &[u8] = data;
        js! { @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var pixels = null;
            var pixels_u8 = @{TypedArray::from(data_u8)};
            var pixels_kind = @{array_kind};
            if (pixels_kind == 1) {
                pixels = new Uint8Array(pixels_u8);
            } else if (pixels_kind == 2) {
                pixels = new Uint16Array(pixels_u8);
            } else {
                pixels = new Float32Array(pixels_u8);
            }
            ctx.readPixels(@{x}, @{y}, @{width} @{height},
                @{format as u32}, @{pixel_type as u32}, pixels);
        };
        self.check_gl_error("read_pixels");
    }

    fn renderbuffer_storage(&self, format: RenderbufferFormat, width: u32, height: u32) {
        self.log("renderbuffer_storage");
        js! { @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            ctx.renderbufferStorage(ctx.RENDERBUFFER, @{format as u32}, @{width}, @{height});
        };
        self.check_gl_error("renderbuffer_storage");
    }

    // S

    fn scissor(&self, x: u32, y: u32, width: u32, height: u32) {
        no_return_impl!( self; scissor; "scissor"; x, y, width, height );
    }

    fn shader_source(&self, shader: &WebGLShader, source: &str) {
        self.log("shader_source");
        js! { @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var s = Module.gl.get(@{shader.0});
            ctx.shaderSource(s, @{source});
        }
        self.check_gl_error("shader_source");
    }

    fn stencil_func_separate(&self, face: FaceMode, func: CompareOp,
                                 ref_value: i32, mask: u32
    ) {
        no_return_impl!( self; stencilFuncSeparate; "stencil_func_separate";
            face as u32, func as u32, ref_value, mask);
    }

    fn stencil_mask_separate(&self, face: FaceMode, mask: u32) {
        no_return_impl!( self; stencilMaskSeparate; "stencil_mask_separate";
            face as u32, mask);
    }

    fn stencil_op_separate(&self, face: FaceMode, sfail: StencilOp, zfail: StencilOp,
                               zpass: StencilOp) {
        no_return_impl!( self; stencilOpSeparate; "stencil_op_separate";
            face as u32, sfail as u32, zfail as u32, zpass as u32);
    }

    // T

    fn tex_image2d<T: Sized>(&self, target: TextureTarget, level: u8, format: PixelFormat,
                                 width: u32, height: u32, pixel_type: PixelType, data: &[T]) {
        self.log("tex_image2d");
        let data_u8 = slice_as_u8(data);
        js! { @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var ar = TypedArray::from(@{data_u8});
            var jsdata = ar.length == 0 ? null : ar;
            ctx.texImage2D(@{target as u32}, @{level as u32}, @{format as u32},
                @{width}, @{height}, 0, @{format as u32}, @{pixel_type as u32},
                jsdata);
        }
        self.check_gl_error("tex_image2d");
    }

    fn tex_sub_image2d<T: Sized>(&self, target: TextureTarget, level: u8,
                                     xoffset: u32, yoffset: u32, width: u32, height: u32,
                                     format: PixelFormat, pixel_type: PixelType, data: &[T]
    ) {
        self.log("tex_sub_image2d");
        let data_u8 = slice_as_u8(data);
        js!{ @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            ctx.texSubImage2D(@{target as u32}, @{level as u32},
                @{xoffset}, @{yoffset}, @{width}, @{height},
                @{format as u32}, @{pixel_type as u32}, @{ TypedArray::from(data_u8) });
        };
        self.check_gl_error("tex_sub_image2d");
    }

    fn tex_parameter(&self, target: TextureTarget, name: ParamName, value: ParamValue) {
        no_return_impl!( self; texParameteri; "tex_parameter";
            target as u32, name as u32, value as u32);
    }

    // U

    fn uniform_1f(&self, loc: &WebGLUniformLocation, value: f32) {
        let _v = value;
        uniform_impl!{ self; loc; uniform1f; "uniform_1f"; value }
    }

    fn uniform_1i(&self, loc: &WebGLUniformLocation, value: i32) {
        let _v = value;
        uniform_impl!{ self; loc; uniform1i; "uniform_1i"; value }
    }

    fn uniform_2f(&self, loc: &WebGLUniformLocation, value: (f32, f32)) {
        let _v = value.0;
        uniform_impl!{ self; loc; uniform2f; "uniform_2f"; value.0, value.1 }
    }

    fn uniform_2i(&self, loc: &WebGLUniformLocation, value: (i32, i32)) {
        let _v = value.0;
        uniform_impl!{ self; loc; uniform2i; "uniform_2i"; value.0, value.1 }
    }

    fn uniform_3f(&self, loc: &WebGLUniformLocation, value: (f32, f32, f32)) {
        let _v = value.0;
        uniform_impl!{ self; loc; uniform3f; "uniform_3f"; value.0, value.1, value.2 }
    }

    fn uniform_3i(&self, loc: &WebGLUniformLocation, value: (i32, i32, i32)) {
        let _v = value.0;
        uniform_impl!{ self; loc; uniform3i; "uniform_3i"; value.0, value.1, value.2 }
    }

    fn uniform_4f(&self, loc: &WebGLUniformLocation, value: (f32, f32, f32, f32)) {
        let _v = value.0;
        uniform_impl!{ self; loc; uniform4f; "uniform_4f"; value.0, value.1, value.2, value.3 }
    }

    fn uniform_4i(&self, loc: &WebGLUniformLocation, value: (i32, i32, i32, i32)) {
        let _v = value.0;
        uniform_impl!{ self; loc; uniform4i; "uniform_4i"; value.0, value.1, value.2, value.3 }
    }

    fn uniform_matrix_2f(&self, loc: &WebGLUniformLocation, value: &[f32]) {
        self.log("uniform_matrix_2fv");
        let value_f32 = value[0..4];
        js!{ @(no_return)
            var ar = @{value_f32};
            var ctx = Module.gl.get(@{self.reference});
            var u = Module.gl.get(@{loc.reference});
            ctx.uniformMatrix2fv(u, 1, false, ar);
        };
        self.check_gl_error("uniform_matrix_2fv");
    }

    fn uniform_matrix_3f(&self, loc: &WebGLUniformLocation, value: &[f32]) {
        self.log("uniform_matrix_3fv");
        let value_f32 = value[0..9];
        js!{ @(no_return)
            var ar = @{value_f32};
            var ctx = Module.gl.get(@{self.reference});
            var u = Module.gl.get(@{loc.reference});
            ctx.uniformMatrix3fv(u, 1, false, ar);
        };
        self.check_gl_error("uniform_matrix_3fv");
    }

    fn uniform_matrix_4f(&self, loc: &WebGLUniformLocation, value: &[f32]) {
        self.log("uniform_matrix_4fv");
        let value_f32 = value[0..16];
        js!{ @(no_return)
            var ar = @{value_f32};
            var ctx = Module.gl.get(@{self.reference});
            var u = Module.gl.get(@{loc.reference});
            ctx.uniformMatrix4fv(u, 1, false, ar);
        };
        self.check_gl_error("uniform_matrix_4fv");
    }

    /// bind a program to the current state.
    fn use_program(&self, program: &WebGLProgram) {
        self.log("use_program");
        js! { @(no_return)
            var ctx = Module.gl.get(@{self.reference});
            var p = Module.gl.get(@{program.0});
            ctx.useProgram(p);
        };
        self.check_gl_error("use_program");
    }

    // V

    fn vertex_attrib_pointer(
        &self,
        location: u32,
        size: u8,
        kind: DataType,
        normalized: bool,
        stride: u32,
        offset: u32,
    ) {
        self.log("vertex_attrib_pointer");
        js! {
            var ctx = Module.gl.get(@{self.reference});
            ctx.vertexAttribPointer(
                @{location}, @{size as u32}, @{kind as u32}, @{normalized}, @{stride}, @{offset});
        };
        self.check_gl_error("vertex_attrib_pointer");
    }

    fn viewport(&self, x: u32, y: u32, width: u32, height: u32) {
        no_return_impl!( self; viewport; "viewport"; x, y, width, height );
    }
}

unsafe fn f32_slice<'a, T>(_life: &'a T, value: *const f32, count: usize) -> UnsafeTypedArray<f32> {
    let slice = std::slice::from_raw_parts::<f32>(value, count);
    UnsafeTypedArray::new(slice)
}

pub(crate) fn slice_as_u8<T: Sized>(data: &[T]) -> &[u8] {
    unsafe {
        std::slice::from_raw_parts(data.as_ptr() as *const u8, slice_length(data))
    }
}