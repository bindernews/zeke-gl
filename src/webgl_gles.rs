use glad_gles2::gl::{self, GLsizei, GLint, GLenum, GLuint, GLvoid};
use super::common::*;
use super::glenum::{*, enumerations as gle};
use super::helper::*;
use super::api::{IGLContext, WebGLActiveInfo, WebGLParameterOut};
use std::ffi::{CStr, CString, c_void};
use std::ptr::{self, null};
use std::collections::HashMap;
use std::cell::{Cell, RefCell};

pub type Reference = u32;
pub type BoxDebugCallback = Box<dyn Fn(Severity, String)>;

pub struct GLContext {
    has_oes_vertex_array_object: bool,
    extensions: String,
    bound_buffer: Cell<Reference>,
    vao: RefCell<VAOImpl>,
    debug_callback: RefCell<BoxDebugCallback>,
}

// OpenGL ES on the Raspberry Pi doesn't support VAOs, so we emulate them if they're
// not supported natively.

struct VAOImpl {
    vaos: HashMap<Reference, VAOData>,
    current: u32,
    next_id: u32,
}

struct VAOData {
    bindings: HashMap<u32, VAOBinding>,
}

struct VAOBinding {
    buffer: Reference,
    location: u32,
    size: u8,
    kind: DataType,
    normalized: bool,
    stride: u32,
    offset: u32,
}

/// gl::GetString convenient wrapper
fn get_string(param: u32) -> String {
    return unsafe {
        let sp = gl::GetString(param) as *const i8;
        if sp == null() {
            String::new()
        } else {
            let data = CStr::from_ptr(sp)
                .to_bytes()
                .to_vec();
            String::from_utf8(data).unwrap()
        }
    };
}

macro_rules! uniform_impl {
    ( $me:ident; $loc:ident; $real_name:ident; $error_name:expr; $($decomp:tt)*) => {
        unsafe {
            gl:: $real_name( $loc.reference as _, $($decomp)* );
        }
        $me.check_gl_error($error_name);
    };
}

impl WebGLRenderingContext {
    pub fn new<F>(loadfn: F) -> Self
    where
        F: FnMut(&'static str) -> *const c_void
    {
        gl::load(loadfn);
        Self {
            ctx: GLContext::new(),
        }
    }
}

impl GLContext {
    pub fn new() -> GLContext {
        if cfg!(feature = "emulate-vao") {
            println!("Emulating VAOs");
        }
        GLContext {
            has_oes_vertex_array_object: !cfg!(feature = "emulate-vao"),
            extensions: get_string(gl::GL_EXTENSIONS),
            bound_buffer: Cell::new(0),
            vao: RefCell::new(VAOImpl {
                vaos: HashMap::new(),
                current: 0,
                next_id: 1,
            }),
            debug_callback: RefCell::new(Box::new(default_debug_callback)),
        }
    }

    /// panics with a proper message if the last OpenGL call returned an error
    fn check_gl_error(&self, msg: &str) {
        if cfg!(feature = "check-all-errors") {
            let err = unsafe { gl::GetError() };
            if err != ErrorCode::NoError as u32 {
                let emsg = match ErrorCode::from_u32(err) {
                    Some(c) => c.as_str(),
                    None => "unknown error",
                };
                panic!(format!("GLError: {} {} ({})",  msg, err, emsg));
            }
        }
    }

    pub fn get_extensions(&self) -> &str {
        &self.extensions
    }

    fn get_integer(&self, parameter: u32) -> i32 {
        let mut data = 0i32;
        unsafe { gl::GetIntegerv(parameter  as _ , &mut data); }
        data
    }

    pub fn set_debug_callback(&self, callback: Option<BoxDebugCallback>)
    {
        if let Some(cb) = callback {
            let mut debug_callback = self.debug_callback.borrow_mut();
            *debug_callback = cb;
            unsafe {
                let callback_ptr = std::mem::transmute::<&BoxDebugCallback, *const c_void>(&*debug_callback);
                gl::DebugMessageCallbackKHR(gl_debug_callback, callback_ptr);
                gl::Enable(gl::GL_DEBUG_OUTPUT_KHR);
            }
        } else {
            unsafe {
                gl::Disable(gl::GL_DEBUG_OUTPUT_KHR);
            }
        }
    }
}

impl IGLContext for GLContext {
    //////////
    // WebGL functions in alphabetical order
    //////////

    // A

    fn active_texture(&self, unit: u32) {
        unsafe {
            gl::ActiveTexture(gle::GL_TEXTURE0 + unit);
        }
        self.check_gl_error("active_texture");
    }

    /// attach a shader to a program. A program must have two shaders : vertex and fragment shader.
    fn attach_shader(&self, program: &WebGLProgram, shader: &WebGLShader) {
        unsafe {
            gl::AttachShader(program.0, shader.0);
        }
        self.check_gl_error("attach_shader");
    }

    // B

    /// associate a generic vertex attribute index with a named attribute
    fn bind_attrib_location(&self, program: &WebGLProgram, loc: u32, name: &str) {
        let c_name = CString::new(name).unwrap();
        unsafe {
            gl::BindAttribLocation(program.0 as _, loc as _, c_name.as_ptr());
        }
        self.check_gl_error("bind_attrib_location");
    }

    fn bind_buffer(&self, kind: BufferKind, buffer: Option<&WebGLBuffer>) {
        let rf = if let Some(r) = buffer { r.0 } else { 0 };
        unsafe {
            gl::BindBuffer(kind as _, rf);
        }
        self.check_gl_error("bind_buffer");
        self.bound_buffer.set(rf);
    }

    /// bind a framebuffer to the current state
    fn bind_framebuffer(&self, fb: Option<&WebGLFramebuffer>) {
        let rf = if let Some(r) = fb { r.0 } else { 0 };
        unsafe {
            gl::BindFramebuffer(gl::GL_FRAMEBUFFER, rf);
        }
        self.check_gl_error("bind_framebuffer");
    }

    fn bind_renderbuffer(&self, rb: Option<&WebGLRenderbuffer>) {
        let rf = if let Some(r) = rb { r.0 } else { 0 };
        unsafe {
            gl::BindRenderbuffer(gl::GL_RENDERBUFFER, rf);
        }
        self.check_gl_error("bind_renderbuffer");
    }

    fn bind_texture(&self, target: TextureTarget, tex: Option<&WebGLTexture>) {
        let rf = if let Some(r) = tex { r.0 } else { 0 };
        unsafe {
            gl::BindTexture(target as _, rf);
        }
        self.check_gl_error("bind_texture");
    }

    /// bind a vertex array object to current state
    fn bind_vertex_array(&self, vao: Option<&WebGLVertexArray>) {
        let rf = if let Some(r) = vao { r.0 } else { 0 };
        if self.has_oes_vertex_array_object {
            unsafe {
                gl::BindVertexArray(rf);
            }
        } else {
            let mut m_vao = self.vao.borrow_mut();
            m_vao.current = rf;
            // Bind the emulated VAO
            let vaodata = &m_vao.vaos[&rf];
            for (_, v) in vaodata.bindings.iter() {
                unsafe {
                    gl::BindBuffer(BufferKind::Array as _, v.buffer);
                    gl::VertexAttribPointer(
                        v.location as _,
                        v.size as _,
                        v.kind as _,
                        v.normalized as _,
                        v.stride as _,
                        v.offset as _,
                    );
                }
            }
        }
        self.check_gl_error("bind_vertex_array");
    }

    fn blend_color(&self, r: f32, g: f32, b: f32, a: f32) {
        unsafe {
            gl::BlendColor(r, g, b, a);
        }
        self.check_gl_error("blend_color");
    }

    fn blend_equation_separate(&self, mode_rgb: BlendMode, mode_alpha: BlendMode) {
        unsafe {
            gl::BlendEquationSeparate(mode_rgb as _, mode_alpha as _);
        }
        self.check_gl_error("blend_equation_separate");
    }

    fn blend_func(&self, sfactor: BlendFactor, dfactor: BlendFactor) {
        unsafe {
            gl::BlendFunc(sfactor as _, dfactor as _);
        }
        self.check_gl_error("blend_func");
    }

    fn blend_func_separate(&self, src_rgb: BlendFactor, dst_rgb: BlendFactor, 
                        src_a: BlendFactor, dst_a: BlendFactor) {
        unsafe {
            gl::BlendFuncSeparate(src_rgb as _, dst_rgb as _, src_a as _, dst_a as _);
        }
        self.check_gl_error("blend_func_separate");
    }

    fn buffer_data<T: Sized>(&self, kind: BufferKind, data: &[T], usage: BufferUsage) {
        unsafe {
            gl::BufferData(kind as _, slice_length(data) as _, data.as_ptr() as _,
                           usage as _);
        }
        self.check_gl_error("buffer_data");
    }

    fn buffer_sub_data<T: Sized>(&self, kind: BufferKind, offset: u32, data: &[T]) {
        unsafe {
            gl::BufferSubData(kind as _, offset as _, slice_length(data) as _,
                              data.as_ptr() as _);
        }
        self.check_gl_error("buffer_sub_data");
    }

    // C

    fn check_framebuffer_status(&self) -> FramebufferError {
        let code = unsafe { gl::CheckFramebufferStatus(gl::GL_FRAMEBUFFER) };
        FramebufferError::from_u32(code)
    }

    fn clear(&self, mask: u32) {
        unsafe { gl::Clear(mask); }
        self.check_gl_error("clear");
    }

    /// specify clear values for the color buffers
    fn clear_color(&self, r: f32, g: f32, b: f32, a: f32) {
        unsafe {
            gl::ClearColor(r, g, b, a);
        }
        self.check_gl_error("clear_color");
    }

    fn clear_depth(&self, depth: f32) {
        unsafe { gl::ClearDepthf(depth); }
        self.check_gl_error("clear_depth");
    }

    fn clear_stencil(&self, s: i32) {
        unsafe { gl::ClearStencil(s); }
        self.check_gl_error("clear_stencil");
    }

    fn color_mask(&self, r: bool, g: bool, b: bool, a: bool) {
        unsafe { gl::ColorMask(r as _, g as _, b as _, a as _); };
        self.check_gl_error("color_mask");
    }

    fn compile_shader(&self, shader: &WebGLShader) -> Result<(), String>{
        unsafe {
            gl::CompileShader(shader.0);

            // Get the compile status
            let mut status = gl::GL_FALSE as gl::types::GLint;
            gl::GetShaderiv(shader.0, gl::GL_COMPILE_STATUS, &mut status);

            // Fail on error
            if status != (gl::GL_TRUE as gl::types::GLint) {
                let mut len = 0;
                gl::GetShaderiv(shader.0, gl::GL_INFO_LOG_LENGTH, &mut len);
                let mut buf = Vec::with_capacity(len as usize);
                buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
                gl::GetShaderInfoLog(
                    shader.0,
                    len,
                    ptr::null_mut(),
                    buf.as_mut_ptr() as *mut gl::types::GLchar,
                );

                return match String::from_utf8(buf) {
                    Ok(s) => Err(s),
                    Err(_) => Err("reason unknown".into()),
                };
            }
        }
        self.check_gl_error("compile_shader");
        Ok(())
    }

    fn copy_tex_image2d(&self, target: TextureTarget, level: u8, format: PixelFormat,
                        x: u32, y: u32, width: u32, height: u32) {
        unsafe {
            gl::CopyTexImage2D(target as _, level as _, format as _,
                x as _, y as _, width as _, height as _, 0);
        }
        self.check_gl_error("copy_tex_image2d");
    }

    fn create_buffer(&self) -> WebGLBuffer {
        let mut buffer = WebGLBuffer(0);
        unsafe {
            gl::GenBuffers(1, &mut buffer.0);
        }
        self.check_gl_error("create_buffer");
        buffer
    }

    /// create a new framebuffer
    fn create_framebuffer(&self) -> WebGLFramebuffer {
        let mut fb = WebGLFramebuffer(0);
        unsafe {
            gl::GenFramebuffers(1, &mut fb.0);
        }
        self.check_gl_error("create_framebuffer");
        fb
    }

    fn create_program(&self) -> WebGLProgram {
        let program = unsafe { WebGLProgram(gl::CreateProgram()) };
        self.check_gl_error("create_program");
        program
    }

    fn create_shader(&self, kind: ShaderKind) -> WebGLShader {
        let shader = unsafe { WebGLShader(gl::CreateShader(kind as _)) };
        self.check_gl_error("create_shader");
        shader
    }

    fn create_renderbuffer(&self) -> WebGLRenderbuffer {
        let mut rb = WebGLRenderbuffer(0);
        unsafe {
            gl::GenRenderbuffers(1, &mut rb.0);
        }
        self.check_gl_error("create_renderbuffer");
        rb
    }

    fn create_texture(&self) -> WebGLTexture {
        let mut id = 0u32;
        unsafe {
            gl::GenTextures(1, &mut id);
        }
        self.check_gl_error("create_texture");
        WebGLTexture(id)
    }

    /// create a vertex array object
    fn create_vertex_array(&self) -> WebGLVertexArray {
        if self.has_oes_vertex_array_object {
            let mut vao = WebGLVertexArray(0);
            unsafe {
                gl::GenVertexArrays(1, &mut vao.0);
            }
            self.check_gl_error("create_vertex_array");
            vao
        } else {
            let mut m_vao = self.vao.borrow_mut();
            let vao = WebGLVertexArray(m_vao.next_id);
            m_vao.next_id += 1;
            vao
        }
    }

    fn cull_face(&self, mode: FaceMode) {
        unsafe {
            gl::CullFace(mode as _);
        }
        self.check_gl_error("cull_face");
    }

    // D

    fn delete_buffer(&self, buffer: &WebGLBuffer) {
        unsafe {
            gl::DeleteBuffers(1, &buffer.0);
        }
        self.check_gl_error("delete_buffer");
    }

    /// destroy a framebuffer
    fn delete_framebuffer(&self, fb: &WebGLFramebuffer) {
        unsafe {
            gl::DeleteFramebuffers(1, &fb.0);
        }
        self.check_gl_error("delete_framebuffer");
    }

    fn delete_program(&self, program: &WebGLProgram) {
        unsafe { gl::DeleteProgram(program.0) };
        self.check_gl_error("delete_program");
    }

    fn delete_renderbuffer(&self, rb: &WebGLRenderbuffer) {
        unsafe {
            gl::DeleteRenderbuffers(1, &rb.0);
        }
        self.check_gl_error("delete_renderbuffer");
    }

    fn delete_shader(&self, shader: &WebGLShader) {
        unsafe { gl::DeleteShader(shader.0); }
        self.check_gl_error("delete_shader");
    }

    fn delete_texture(&self, tex: &WebGLTexture) {
        unsafe {
            gl::DeleteTextures(1, &tex.0);
        }
        self.check_gl_error("delete_texture");
    }

    /// destroy a vertex array object
    fn delete_vertex_array(&self, vao: &WebGLVertexArray) {
        if self.has_oes_vertex_array_object {
            unsafe {
                gl::DeleteVertexArrays(1, &vao.0);
            }
            self.check_gl_error("delete_vertex_array");
        } else {
            let mut m_vao = self.vao.borrow_mut();
            m_vao.vaos.remove(&vao.0);
        }
    }

    fn depth_func(&self, func: CompareOp) {
        unsafe {
            gl::DepthFunc(func as _);
        }
        self.check_gl_error("depth_func");
    }

    fn depth_mask(&self, flag: bool) {
        unsafe {
            gl::DepthMask(flag as _);
        }
        self.check_gl_error("depth_mask");
    }

    fn depth_range(&self, z_near: f32, z_far: f32) {
        unsafe {
            gl::DepthRangef(z_near, z_far);
        }
        self.check_gl_error("depth_range");
    }

    fn detach_shader(&self, program: &WebGLProgram, shader: &WebGLShader) {
        unsafe {
            gl::DetachShader(program.0, shader.0);
        }
        self.check_gl_error("deatch_shader");
    }

    /// disable GL capabilities.
    ///
    /// flag should be one of [`Flag`]
    fn disable(&self, flag: Flag) {
        unsafe {
            gl::Disable(flag as _);
        }
        self.check_gl_error("disable");
    }

    fn disable_vertex_attrib_array(&self, loc: u32) {
        unsafe {
            gl::DisableVertexAttribArray(loc as _);
        }
        self.check_gl_error("disable_vertex_attrib_array");
    }

    /// render primitives from array data
    fn draw_arrays(&self, mode: Primitives, first: u32, count: u32) {
        unsafe {
            gl::DrawArrays(mode as _, first as _, count as _);
        };
        self.check_gl_error("draw_arrays");
    }

    /// render primitives from indexed array data
    fn draw_elements(&self, mode: Primitives, count: usize, kind: DataType, offset: u32) {
        unsafe {
            gl::DrawElements(mode as _, count as _, kind as _, offset as _);
        };
        self.check_gl_error("draw_elements");
    }

    // E

    /// enable GL capabilities.
    ///
    /// flag should be one of [`Flag`]
    fn enable(&self, flag: Flag) {
        unsafe {
            gl::Enable(flag as _);
        }
        self.check_gl_error("enable");
    }

    /// enable a generic vertex attribute array
    fn enable_vertex_attrib_array(&self, location: u32) {
        unsafe {
            gl::EnableVertexAttribArray(location as _);
        }
        self.check_gl_error("enable_vertex_attrib_array");
    }

    // F

    fn finish(&self) {
        unsafe { gl::Finish(); }
        self.check_gl_error("finish");
    }

    fn flush(&self) {
        unsafe { gl::Flush(); }
        self.check_gl_error("flush");
    }

    fn framebuffer_renderbuffer(&self, attachment: Attachment, rb: &WebGLRenderbuffer) {
        unsafe {
            gl::FramebufferRenderbuffer(gl::GL_FRAMEBUFFER, attachment as _, gl::GL_RENDERBUFFER, rb.0 as _);
        }
        self.check_gl_error("framebuffer_renderbuffer");
    }

    /// attach a texture to a framebuffer
    fn framebuffer_texture2d(
        &self,
        attachment: Attachment,
        textarget: TextureTarget,
        texture: &WebGLTexture,
        level: i32,
    ) {
        unsafe {
            gl::FramebufferTexture2D(
                gl::GL_FRAMEBUFFER as _,
                attachment as u32,
                textarget as u32,
                texture.0,
                level,
            );
        }
        self.check_gl_error("framebuffer_texture2d");
    }

    fn front_face(&self, mode: WindingMode) {
        unsafe { gl::FrontFace(mode as _); }
        self.check_gl_error("front_face");
    }

    // G

    fn generate_mipmap(&self, target: TextureTarget) {
        unsafe { gl::GenerateMipmap(target as _); }
        self.check_gl_error("generate_mipmap");
    }

    fn get_active_attrib(&self, program: &WebGLProgram, location: u32) -> Option<WebGLActiveInfo> {
        unsafe {
            let mut max_length: GLint = 0;
            gl::GetProgramiv(program.0, gl::GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &mut max_length);
            let mut buffer = Vec::new();
            buffer.resize(max_length as usize, 0u8);

            let mut size: GLint = 0;
            let mut kind: GLenum = 0;
            let mut out_length: GLsizei = 0;
            gl::GetActiveAttrib(program.0, location, buffer.len() as _, &mut out_length, &mut size, &mut kind,
                buffer.as_mut_ptr() as *mut i8);
            if gl::GetError() != gl::GL_NO_ERROR {
                None
            } else {
                let name: String = CStr::from_bytes_with_nul(&buffer).ok()?.to_str().ok()?.into();
                Some(WebGLActiveInfo {
                    name,
                    size: size as _,
                    kind: ActiveType::from_u32(kind).ok()?,
                })
            }
        }
    }

    fn get_active_uniform(&self, program: &WebGLProgram, index: u32) -> Option<WebGLActiveInfo> {
        unsafe {
            let mut max_length: GLint = 0;
            gl::GetProgramiv(program.0, gl::GL_ACTIVE_UNIFORM_MAX_LENGTH, &mut max_length);
            let mut buffer = Vec::new();
            buffer.resize(max_length as usize, 0u8);

            let mut size: GLint = 0;
            let mut kind: GLenum = 0;
            let mut out_length: GLsizei = 0;
            gl::GetActiveUniform(program.0, index as _, max_length as _, &mut out_length, &mut size, &mut kind,
                buffer.as_mut_ptr() as *mut i8);
            if gl::GetError() != gl::GL_NO_ERROR {
                None
            } else {
                let name: String = CStr::from_bytes_with_nul(&buffer).ok()?.to_str().ok()?.into();
                Some(WebGLActiveInfo {
                    name,
                    size: size as _,
                    kind: ActiveType::from_u32(kind).ok()?,
                })
            }
        }
    }

    /// return the location of an attribute variable
    fn get_attrib_location(&self, program: &WebGLProgram, name: &str) -> Option<u32> {
        let c_name = CString::new(name).unwrap();
        unsafe {
            let location = gl::GetAttribLocation(program.0 as _, c_name.as_ptr());
            self.check_gl_error("get_attrib_location");
            if location == -1 {
                return None;
            }
            return Some(location as _);
        }
    }

    fn get_error(&self) -> u32 {
        unsafe {
            gl::GetError() as u32
        }
    }

    fn get_parameter(&self, parameter: u32) -> WebGLParameterOut {
        use gle::*;
        // https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/getParameter
        match parameter {
            // GLint
            gle::GL_ALPHA_BITS | gle::GL_BLUE_BITS | gle::GL_DEPTH_BITS | gle::GL_GREEN_BITS 
            | gle::GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS | gle::GL_MAX_CUBE_MAP_TEXTURE_SIZE
            | gle::GL_MAX_FRAGMENT_UNIFORM_VECTORS | gle::GL_MAX_RENDERBUFFER_SIZE
            | gle::GL_MAX_TEXTURE_IMAGE_UNITS | gle::GL_MAX_TEXTURE_SIZE | gle::GL_MAX_VARYING_VECTORS
            | gle::GL_MAX_VERTEX_ATTRIBS | gle::GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS
            | gle::GL_MAX_VERTEX_UNIFORM_VECTORS | gle::GL_PACK_ALIGNMENT | gle::GL_RED_BITS
            | gle::GL_SAMPLE_BUFFERS | gle::GL_SAMPLES | gle::GL_STENCIL_BACK_REF
            | gle::GL_STENCIL_BITS | gle::GL_STENCIL_CLEAR_VALUE | gle::GL_STENCIL_REF
            | gle::GL_SUBPIXEL_BITS | gle::GL_UNPACK_ALIGNMENT => {
                WebGLParameterOut::I32(self.get_integer(parameter))
            },
            // GLenum
            gle::GL_BLEND_DST_ALPHA | gle::GL_BLEND_DST_RGB 
            | gle::GL_BLEND_EQUATION_ALPHA | gle::GL_BLEND_EQUATION_RGB
            | gle::GL_BLEND_SRC_ALPHA | gle::GL_BLEND_SRC_RGB | gle::GL_CULL_FACE_MODE
            | gle::GL_DEPTH_FUNC | gle::GL_FRONT_FACE | gle::GL_GENERATE_MIPMAP_HINT
            | gle::GL_IMPLEMENTATION_COLOR_READ_FORMAT | gle::GL_IMPLEMENTATION_COLOR_READ_TYPE
            | gle::GL_STENCIL_BACK_FAIL | gle::GL_STENCIL_BACK_FUNC | gle::GL_STENCIL_PASS_DEPTH_FAIL
            | gle::GL_STENCIL_PASS_DEPTH_PASS | gle::GL_STENCIL_BACK_PASS_DEPTH_FAIL
            | gle::GL_STENCIL_BACK_PASS_DEPTH_PASS => {
                WebGLParameterOut::Enum(self.get_integer(parameter) as u32)
            },
            // GLboolean
            GL_BLEND | GL_CULL_FACE | GL_DEPTH_TEST | GL_DEPTH_WRITEMASK | GL_DITHER
            | GL_POLYGON_OFFSET_FILL | GL_SAMPLE_COVERAGE_INVERT | GL_SCISSOR_TEST
            | GL_STENCIL_TEST => {
                WebGLParameterOut::Bool(self.get_integer(parameter) != 0)
            },
            // Specials
            GL_ACTIVE_TEXTURE => {
                let id = self.get_integer(parameter) as u32 - GL_TEXTURE0;
                WebGLParameterOut::U32(id)
            },
            GL_ARRAY_BUFFER_BINDING | GL_ELEMENT_ARRAY_BUFFER_BINDING => {
                let id = self.get_integer(parameter) as u32;
                WebGLParameterOut::Buffer(WebGLBuffer(id))
            },
            GL_CURRENT_PROGRAM => {
                let id = self.get_integer(parameter) as u32;
                WebGLParameterOut::Program(WebGLProgram(id))
            },
            GL_FRAMEBUFFER_BINDING => {
                let id = self.get_integer(parameter) as u32;
                WebGLParameterOut::Framebuffer(WebGLFramebuffer(id))
            },
            GL_RENDERBUFFER_BINDING => {
                let id = self.get_integer(parameter) as u32;
                WebGLParameterOut::Renderbuffer(WebGLRenderbuffer(id))
            },
            GL_TEXTURE_BINDING_2D | GL_TEXTURE_BINDING_CUBE_MAP => {
                let id = self.get_integer(parameter) as u32;
                WebGLParameterOut::Texture(WebGLTexture(id))
            },
            _ => {
                panic!("`get_parameter` invalid enum {}", parameter);
            },
        }
    }

    fn get_program_parameter(&self, program: &WebGLProgram, param: ProgramParameter) -> i32 {
        unsafe {
            let mut out: GLint = 0;
            gl::GetProgramiv(program.0, param as _, &mut out);
            out
        }
    }

    /// return the location of a uniform variable
    fn get_uniform_location(&self, program: &WebGLProgram, name: &str) -> Option<WebGLUniformLocation> {
        let c_name = CString::new(name).unwrap();
        unsafe {
            let location = gl::GetUniformLocation(program.0 as _, c_name.as_ptr());
            self.check_gl_error("get_uniform_location");
            if location == -1 {
                return None;
            }
            return Some(WebGLUniformLocation {
                reference: location as _,
                // name: name.into(),
            });
        }
    }

    // L

    /// link a program
    fn link_program(&self, program: &WebGLProgram) -> Result<(), String> {
        unsafe {
            gl::LinkProgram(program.0);
            // Get the link status
            let mut status = gl::GL_FALSE as gl::types::GLint;
            gl::GetProgramiv(program.0, gl::GL_LINK_STATUS, &mut status);

            // Fail on error
            if status != (gl::GL_TRUE as gl::types::GLint) {
                let mut len = 0;
                gl::GetProgramiv(program.0, gl::GL_INFO_LOG_LENGTH, &mut len);
                let mut buf = Vec::with_capacity(len as usize);
                buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
                gl::GetProgramInfoLog(
                    program.0,
                    len,
                    ptr::null_mut(),
                    buf.as_mut_ptr() as *mut gl::types::GLchar,
                );

                return match String::from_utf8(buf) {
                    Ok(s) => Err(s),
                    Err(_) => Err("reason unknown".into()),
                };
            }
        }
        self.check_gl_error("link_program");
        Ok(())
    }

    // R

    fn read_pixels(&self, x: u32, y: u32, width: u32, height: u32,
                   format: PixelFormat, pixel_type: PixelType, data: &mut [u8]) {
        let bpp = bytes_per_pixel(format, pixel_type);
        if (bpp * width * height) > data.len() as u32 {
            panic!("Not enough storage space in buffer");
        }
        unsafe {
            gl::ReadPixels(x as _, y as _, width as _, height as _,
                           format as _, pixel_type as _,
                           data.as_mut_ptr() as *mut c_void);
        }
        self.check_gl_error("read_pixels");
    }

    fn renderbuffer_storage(&self, format: RenderbufferFormat, width: u32, height: u32) {
        unsafe {
            gl::RenderbufferStorage(gl::GL_RENDERBUFFER, format as _,
                                    width as _, height as _);
        }
        self.check_gl_error("renderbuffer_storage");
    }

    // S

    fn scissor(&self, x: u32, y: u32, width: u32, height: u32) {
        unsafe {
            gl::Scissor(x as _, y as _, width as _, height as _);
        }
        self.check_gl_error("scissor");
    }

    fn shader_source(&self, shader: &WebGLShader, source: &str) {
        let src = CString::new(source).unwrap();
        unsafe {
            gl::ShaderSource(shader.0, 1, &src.as_ptr(), ptr::null());
        }
        self.check_gl_error("shader_source");
    }

    fn stencil_func_separate(&self, face: FaceMode, func: CompareOp,
                                 ref_value: i32, mask: u32
    ) {
        unsafe {
            gl::StencilFuncSeparate(face as _, func as _, ref_value as _,
                                    mask as _);
        }
        self.check_gl_error("stencil_func_separate");
    }

    fn stencil_mask_separate(&self, face: FaceMode, mask: u32) {
        unsafe {
            gl::StencilMaskSeparate(face as _, mask as _);
        }
        self.check_gl_error("stencil_mask_separate");
    }

    fn stencil_op_separate(&self, face: FaceMode, sfail: StencilOp, zfail: StencilOp,
                               zpass: StencilOp) {
        unsafe {
            gl::StencilOpSeparate(face as _, sfail as _, zfail as _, zpass as _);
        }
        self.check_gl_error("stencil_op_separate");
    }

    // T

    fn tex_image2d<T: Sized>(&self, target: TextureTarget, level: u8, format: PixelFormat,
                                 width: u32, height: u32, pixel_type: PixelType, data: &[T]) {
        let p: *const c_void = if data.len() > 0 {
            data.as_ptr() as *const c_void
        } else {
            null()
        };
        unsafe {
            gl::TexImage2D(target as _, level as _, format as _,
                           width as _, height as _, 0,
                           format as _, pixel_type as _, p);
        }
        self.check_gl_error("tex_image2d");
    }

    fn tex_sub_image2d<T: Sized>(&self, target: TextureTarget, level: u8,
                                     xoffset: u32, yoffset: u32, width: u32, height: u32,
                                     format: PixelFormat, pixel_type: PixelType, data: &[T]
    ) {
        let p: *const c_void = if data.len() > 0 {
            data.as_ptr() as *const c_void
        } else {
            null()
        };
        unsafe {
            gl::TexSubImage2D(target as _, level as _,
                              xoffset as _, yoffset as _,
                              width as _, height as _,
                              format as _, pixel_type as _, p);
        }
        self.check_gl_error("tex_sub_image2d");
    }

    fn tex_parameter(&self, target: TextureTarget, name: ParamName, value: ParamValue) {
        unsafe {
            gl::TexParameteri(target as _, name as _, value as _);
        }
        self.check_gl_error("tex_parameter");
    }

    // U

    fn uniform_1f(&self, loc: &WebGLUniformLocation, value: f32) {
        uniform_impl!{ self; loc; Uniform1f; "uniform_1f"; value }
    }

    fn uniform_1i(&self, loc: &WebGLUniformLocation, value: i32) {
        uniform_impl!{ self; loc; Uniform1i; "uniform_1i"; value }
    }

    fn uniform_2f(&self, loc: &WebGLUniformLocation, value: (f32, f32)) {
        uniform_impl!{ self; loc; Uniform2f; "uniform_2f"; value.0, value.1 }
    }

    fn uniform_2i(&self, loc: &WebGLUniformLocation, value: (i32, i32)) {
        uniform_impl!{ self; loc; Uniform2i; "uniform_2i"; value.0, value.1 }
    }

    fn uniform_3f(&self, loc: &WebGLUniformLocation, value: (f32, f32, f32)) {
        uniform_impl!{ self; loc; Uniform3f; "uniform_3f"; value.0, value.1, value.2 }
    }

    fn uniform_3i(&self, loc: &WebGLUniformLocation, value: (i32, i32, i32)) {
        uniform_impl!{ self; loc; Uniform3i; "uniform_3i"; value.0, value.1, value.2 }
    }

    fn uniform_4f(&self, loc: &WebGLUniformLocation, value: (f32, f32, f32, f32)) {
        uniform_impl!{ self; loc; Uniform4f; "uniform_4f"; value.0, value.1, value.2, value.3 }
    }

    fn uniform_4i(&self, loc: &WebGLUniformLocation, value: (i32, i32, i32, i32)) {
        uniform_impl!{ self; loc; Uniform4i; "uniform_4i"; value.0, value.1, value.2, value.3 }
    }

    fn uniform_matrix_2f(&self, loc: &WebGLUniformLocation, value: &[f32]) {
        unsafe {
            gl::UniformMatrix2fv(loc.reference as _, 1, false as _,
                                 value.as_ptr() as *const f32);
        }
        self.check_gl_error("uniform_matrix_2fv");
    }

    fn uniform_matrix_3f(&self, loc: &WebGLUniformLocation, value: &[f32]) {
        unsafe {
            gl::UniformMatrix3fv(loc.reference as _, 1, false as _,
                                 value.as_ptr() as *const f32);
        }
        self.check_gl_error("uniform_matrix_3fv");
    }

    fn uniform_matrix_4f(&self, loc: &WebGLUniformLocation, value: &[f32]) {
        unsafe {
            gl::UniformMatrix4fv(loc.reference as _, 1, false as _,
                                 value.as_ptr() as *const f32);
        }
        self.check_gl_error("uniform_matrix_4fv");
    }

    /// bind a program to the current state.
    fn use_program(&self, program: &WebGLProgram) {
        unsafe {
            gl::UseProgram(program.0);
        }
        self.check_gl_error("use_program");
    }

    // V

    fn vertex_attrib_pointer(
        &self,
        location: u32,
        size: u8,
        kind: DataType,
        normalized: bool,
        stride: u32,
        offset: u32,
    ) {
        unsafe {
            gl::VertexAttribPointer(
                location as _,
                size as _,
                kind as _,
                normalized as _,
                stride as _,
                offset as _,
            );
        }
        self.check_gl_error("vertex_attrib_pointer");

        // Update the VAO bindings if we're simulating VAOs
        if !self.has_oes_vertex_array_object {
            let mut m_vao = self.vao.borrow_mut();
            let current = m_vao.current;
            if current != 0 {
                let vaod = m_vao.vaos.get_mut(&current).unwrap();
                vaod.bindings.insert(location, VAOBinding {
                    buffer: self.bound_buffer.get(),
                    location,
                    size,
                    kind,
                    normalized,
                    stride,
                    offset,
                });
            }
        }
    }

    fn viewport(&self, x: u32, y: u32, width: u32, height: u32) {
        unsafe {
            gl::Viewport(x as _, y as _, width as _, height as _);
        }
        self.check_gl_error("viewport");
    }

    ///////
    // Alphabetical functions end
    ///////
}


pub extern "system" fn gl_debug_callback (
    source: GLenum,
    type_: GLenum,
    _id: GLuint,
    severity: GLenum,
    _length: GLsizei,
    message: *const gl::GLchar,
    user_param: *mut GLvoid,
) {

    let e_severity = match severity {
        gl::GL_DEBUG_SEVERITY_HIGH_KHR => Severity::High,
        gl::GL_DEBUG_SEVERITY_MEDIUM_KHR => Severity::Medium,
        gl::GL_DEBUG_SEVERITY_LOW_KHR => Severity::Low,
        gl::GL_DEBUG_SEVERITY_NOTIFICATION_KHR => Severity::Info,
        _ => Severity::Unknown,
    };
    let str_severity = match severity {
        gl::GL_DEBUG_SEVERITY_HIGH_KHR => "ERR",
        gl::GL_DEBUG_SEVERITY_MEDIUM_KHR => "WRN",
        gl::GL_DEBUG_SEVERITY_LOW_KHR => "LOW",
        gl::GL_DEBUG_SEVERITY_NOTIFICATION_KHR => "MSG",
        _ => "UNK",
    };
    let _str_source = match source {
        gl::GL_DEBUG_SOURCE_API_KHR => "API",
        gl::GL_DEBUG_SOURCE_APPLICATION_KHR => "APP",
        gl::GL_DEBUG_SOURCE_SHADER_COMPILER_KHR => "SHD",
        gl::GL_DEBUG_SOURCE_THIRD_PARTY_KHR => "3RD",
        gl::GL_DEBUG_SOURCE_WINDOW_SYSTEM_KHR => "WND",
        gl::GL_DEBUG_SOURCE_OTHER_KHR => "OTH",
        _ => "XXX",
    };
    let str_type = match type_ {
        gl::GL_DEBUG_TYPE_ERROR_KHR => "Error",
        gl::GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_KHR => "Deprecated",
        gl::GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_KHR => "Undefined Behaviour",
        gl::GL_DEBUG_TYPE_PERFORMANCE_KHR => "Performance",
        gl::GL_DEBUG_TYPE_PORTABILITY_KHR => "Portability",
        gl::GL_DEBUG_TYPE_OTHER_KHR => "Other",
        gl::GL_DEBUG_TYPE_MARKER_KHR => "Marker",
        gl::GL_DEBUG_TYPE_POP_GROUP_KHR => "Pop Group",
        gl::GL_DEBUG_TYPE_PUSH_GROUP_KHR => "Push Group",
        _ => "Unkown",
    };
    let msg_c = unsafe { CStr::from_ptr(message) };
    let msg = match msg_c.to_str() {
        Ok(v) => v,
        Err(_e) => "Unable to parse message",
    };
    let msg_final = format!("[{}] {} - {} ", str_severity, str_type, msg);
    let callback = unsafe {
        std::mem::transmute::<*mut GLvoid, &BoxDebugCallback>(user_param)
    };
    callback(e_severity, msg_final);
}

fn default_debug_callback(_: Severity, _: String) {}
