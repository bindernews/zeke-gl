error_chain! {
    types {
        Error, ErrorKind, ResultExt, Result;
    }

    foreign_links {
        NullInString(::std::ffi::NulError);
    }

    errors {
        GLError(msg: String) {
            description("An OpenGL error occurred")
            display("OpenGL error: {}", msg)
        }

        ShaderCompile(msg: String) {
            description("Shader compiler error")
            display("Shader compiler error: {}", msg)
        }

        ProgramLink(msg: String) {
            description("Program link error")
            display("Program link error: {}", msg)
        }
    }
}
